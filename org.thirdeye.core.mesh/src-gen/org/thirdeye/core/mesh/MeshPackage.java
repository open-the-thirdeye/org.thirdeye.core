/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.mesh;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.thirdeye.core.dependencies.DependenciesPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.thirdeye.core.mesh.MeshFactory
 * @model kind="package"
 * @generated
 */
public interface MeshPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mesh";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://mesh/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mesh";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MeshPackage eINSTANCE = org.thirdeye.core.mesh.impl.MeshPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.thirdeye.core.mesh.impl.NodeImpl <em>Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.mesh.impl.NodeImpl
	 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getNode()
	 * @generated
	 */
	int NODE = 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.core.mesh.impl.VariableImpl <em>Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.mesh.impl.VariableImpl
	 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getVariable()
	 * @generated
	 */
	int VARIABLE = 5;

	/**
	 * The meta object id for the '{@link org.thirdeye.core.mesh.impl.ModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.mesh.impl.ModelImpl
	 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getModel()
	 * @generated
	 */
	int MODEL = 2;

	/**
	 * The meta object id for the '{@link org.thirdeye.core.mesh.impl.ConnectionImpl <em>Connection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.mesh.impl.ConnectionImpl
	 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getConnection()
	 * @generated
	 */
	int CONNECTION = 3;

	/**
	 * The meta object id for the '{@link org.thirdeye.core.mesh.impl.ProviderImpl <em>Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.mesh.impl.ProviderImpl
	 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getProvider()
	 * @generated
	 */
	int PROVIDER = 4;

	/**
	 * The meta object id for the '{@link org.thirdeye.core.mesh.impl.EdgeImpl <em>Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.mesh.impl.EdgeImpl
	 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getEdge()
	 * @generated
	 */
	int EDGE = 6;

	/**
	 * The meta object id for the '{@link org.thirdeye.core.mesh.impl.TransformationEdgeImpl <em>Transformation Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.mesh.impl.TransformationEdgeImpl
	 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getTransformationEdge()
	 * @generated
	 */
	int TRANSFORMATION_EDGE = 7;

	/**
	 * The meta object id for the '{@link org.thirdeye.core.mesh.impl.MeshImpl <em>Mesh</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.mesh.impl.MeshImpl
	 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getMesh()
	 * @generated
	 */
	int MESH = 0;

	/**
	 * The feature id for the '<em><b>Model Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESH__MODEL_NODES = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Connection Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESH__CONNECTION_NODES = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Provider Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESH__PROVIDER_NODES = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Integrator Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESH__INTEGRATOR_EDGES = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>From Model Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESH__FROM_MODEL_EDGES = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>To Provider Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESH__TO_PROVIDER_EDGES = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>From Connection Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESH__FROM_CONNECTION_EDGES = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Edges</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESH__EDGES = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESH__NODES = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Mesh</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESH_FEATURE_COUNT = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 9;

	/**
	 * The operation id for the '<em>Clone</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESH___CLONE = DependenciesPackage.ERUNNABLE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Print</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESH___PRINT = DependenciesPackage.ERUNNABLE_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Sync</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESH___SYNC__MESH = DependenciesPackage.ERUNNABLE_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Mesh</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESH_OPERATION_COUNT = DependenciesPackage.ERUNNABLE_OPERATION_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__VARIABLES = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__NAME = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_FEATURE_COUNT = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE___RUN = DependenciesPackage.ERUNNABLE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE___CALC = DependenciesPackage.ERUNNABLE_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Logic</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE___LOGIC = DependenciesPackage.ERUNNABLE_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_OPERATION_COUNT = DependenciesPackage.ERUNNABLE_OPERATION_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__VARIABLES = NODE__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__NAME = NODE__NAME;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_FEATURE_COUNT = NODE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL___RUN = NODE___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL___CALC = NODE___CALC;

	/**
	 * The operation id for the '<em>Logic</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL___LOGIC = NODE___LOGIC;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__VARIABLES = NODE__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__NAME = NODE__NAME;

	/**
	 * The number of structural features of the '<em>Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_FEATURE_COUNT = NODE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION___RUN = NODE___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION___CALC = NODE___CALC;

	/**
	 * The operation id for the '<em>Logic</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION___LOGIC = NODE___LOGIC;

	/**
	 * The number of operations of the '<em>Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER__VARIABLES = NODE__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER__NAME = NODE__NAME;

	/**
	 * The number of structural features of the '<em>Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER_FEATURE_COUNT = NODE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER___RUN = NODE___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER___CALC = NODE___CALC;

	/**
	 * The operation id for the '<em>Logic</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER___LOGIC = NODE___LOGIC;

	/**
	 * The operation id for the '<em>Apply</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER___APPLY__OBJECT = NODE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER_OPERATION_COUNT = NODE_OPERATION_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__FEATURE = 0;

	/**
	 * The feature id for the '<em><b>Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__NODE = 1;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__UNIT = 2;

	/**
	 * The feature id for the '<em><b>Accessors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__ACCESSORS = 3;

	/**
	 * The number of structural features of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FEATURE_COUNT = 4;

	/**
	 * The operation id for the '<em>Get</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE___GET__INT = 0;

	/**
	 * The operation id for the '<em>Set</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE___SET__INT_OBJECT = 1;

	/**
	 * The number of operations of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_OPERATION_COUNT = 2;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__FROM = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__TO = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_FEATURE_COUNT = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE___RUN = DependenciesPackage.ERUNNABLE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE___CALC = DependenciesPackage.ERUNNABLE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_OPERATION_COUNT = DependenciesPackage.ERUNNABLE_OPERATION_COUNT + 2;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_EDGE__FROM = EDGE__FROM;

	/**
	 * The feature id for the '<em><b>To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_EDGE__TO = EDGE__TO;

	/**
	 * The feature id for the '<em><b>Providers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_EDGE__PROVIDERS = EDGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Transformation Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_EDGE_FEATURE_COUNT = EDGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_EDGE___RUN = EDGE___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_EDGE___CALC = EDGE___CALC;

	/**
	 * The number of operations of the '<em>Transformation Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_EDGE_OPERATION_COUNT = EDGE_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.thirdeye.core.mesh.impl.AccessorImpl <em>Accessor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.mesh.impl.AccessorImpl
	 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getAccessor()
	 * @generated
	 */
	int ACCESSOR = 8;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESSOR__INDEX = 0;

	/**
	 * The feature id for the '<em><b>Var</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESSOR__VAR = 1;

	/**
	 * The number of structural features of the '<em>Accessor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESSOR_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Accessor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESSOR_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.mesh.Node <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node</em>'.
	 * @see org.thirdeye.core.mesh.Node
	 * @generated
	 */
	EClass getNode();

	/**
	 * Returns the meta object for the containment reference list '{@link org.thirdeye.core.mesh.Node#getVariables <em>Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variables</em>'.
	 * @see org.thirdeye.core.mesh.Node#getVariables()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_Variables();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.mesh.Node#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.thirdeye.core.mesh.Node#getName()
	 * @see #getNode()
	 * @generated
	 */
	EAttribute getNode_Name();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.mesh.Node#run() <em>Run</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Run</em>' operation.
	 * @see org.thirdeye.core.mesh.Node#run()
	 * @generated
	 */
	EOperation getNode__Run();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.mesh.Node#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.core.mesh.Node#calc()
	 * @generated
	 */
	EOperation getNode__Calc();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.mesh.Node#logic() <em>Logic</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Logic</em>' operation.
	 * @see org.thirdeye.core.mesh.Node#logic()
	 * @generated
	 */
	EOperation getNode__Logic();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.mesh.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable</em>'.
	 * @see org.thirdeye.core.mesh.Variable
	 * @generated
	 */
	EClass getVariable();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.mesh.Variable#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Feature</em>'.
	 * @see org.thirdeye.core.mesh.Variable#getFeature()
	 * @see #getVariable()
	 * @generated
	 */
	EAttribute getVariable_Feature();

	/**
	 * Returns the meta object for the container reference '{@link org.thirdeye.core.mesh.Variable#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Node</em>'.
	 * @see org.thirdeye.core.mesh.Variable#getNode()
	 * @see #getVariable()
	 * @generated
	 */
	EReference getVariable_Node();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.mesh.Variable#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unit</em>'.
	 * @see org.thirdeye.core.mesh.Variable#getUnit()
	 * @see #getVariable()
	 * @generated
	 */
	EAttribute getVariable_Unit();

	/**
	 * Returns the meta object for the containment reference list '{@link org.thirdeye.core.mesh.Variable#getAccessors <em>Accessors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Accessors</em>'.
	 * @see org.thirdeye.core.mesh.Variable#getAccessors()
	 * @see #getVariable()
	 * @generated
	 */
	EReference getVariable_Accessors();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.mesh.Variable#get(int) <em>Get</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get</em>' operation.
	 * @see org.thirdeye.core.mesh.Variable#get(int)
	 * @generated
	 */
	EOperation getVariable__Get__int();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.mesh.Variable#set(int, java.lang.Object) <em>Set</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set</em>' operation.
	 * @see org.thirdeye.core.mesh.Variable#set(int, java.lang.Object)
	 * @generated
	 */
	EOperation getVariable__Set__int_Object();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.mesh.Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see org.thirdeye.core.mesh.Model
	 * @generated
	 */
	EClass getModel();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.mesh.Connection <em>Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection</em>'.
	 * @see org.thirdeye.core.mesh.Connection
	 * @generated
	 */
	EClass getConnection();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.mesh.Provider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provider</em>'.
	 * @see org.thirdeye.core.mesh.Provider
	 * @generated
	 */
	EClass getProvider();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.mesh.Provider#apply(java.lang.Object) <em>Apply</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Apply</em>' operation.
	 * @see org.thirdeye.core.mesh.Provider#apply(java.lang.Object)
	 * @generated
	 */
	EOperation getProvider__Apply__Object();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.mesh.Edge <em>Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Edge</em>'.
	 * @see org.thirdeye.core.mesh.Edge
	 * @generated
	 */
	EClass getEdge();

	/**
	 * Returns the meta object for the reference '{@link org.thirdeye.core.mesh.Edge#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From</em>'.
	 * @see org.thirdeye.core.mesh.Edge#getFrom()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_From();

	/**
	 * Returns the meta object for the reference list '{@link org.thirdeye.core.mesh.Edge#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>To</em>'.
	 * @see org.thirdeye.core.mesh.Edge#getTo()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_To();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.mesh.Edge#run() <em>Run</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Run</em>' operation.
	 * @see org.thirdeye.core.mesh.Edge#run()
	 * @generated
	 */
	EOperation getEdge__Run();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.mesh.Edge#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.core.mesh.Edge#calc()
	 * @generated
	 */
	EOperation getEdge__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.mesh.TransformationEdge <em>Transformation Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transformation Edge</em>'.
	 * @see org.thirdeye.core.mesh.TransformationEdge
	 * @generated
	 */
	EClass getTransformationEdge();

	/**
	 * Returns the meta object for the reference list '{@link org.thirdeye.core.mesh.TransformationEdge#getProviders <em>Providers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Providers</em>'.
	 * @see org.thirdeye.core.mesh.TransformationEdge#getProviders()
	 * @see #getTransformationEdge()
	 * @generated
	 */
	EReference getTransformationEdge_Providers();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.mesh.Accessor <em>Accessor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Accessor</em>'.
	 * @see org.thirdeye.core.mesh.Accessor
	 * @generated
	 */
	EClass getAccessor();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.mesh.Accessor#getIndex <em>Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Index</em>'.
	 * @see org.thirdeye.core.mesh.Accessor#getIndex()
	 * @see #getAccessor()
	 * @generated
	 */
	EAttribute getAccessor_Index();

	/**
	 * Returns the meta object for the container reference '{@link org.thirdeye.core.mesh.Accessor#getVar <em>Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Var</em>'.
	 * @see org.thirdeye.core.mesh.Accessor#getVar()
	 * @see #getAccessor()
	 * @generated
	 */
	EReference getAccessor_Var();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.mesh.Mesh <em>Mesh</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mesh</em>'.
	 * @see org.thirdeye.core.mesh.Mesh
	 * @generated
	 */
	EClass getMesh();

	/**
	 * Returns the meta object for the containment reference list '{@link org.thirdeye.core.mesh.Mesh#getModelNodes <em>Model Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Model Nodes</em>'.
	 * @see org.thirdeye.core.mesh.Mesh#getModelNodes()
	 * @see #getMesh()
	 * @generated
	 */
	EReference getMesh_ModelNodes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.thirdeye.core.mesh.Mesh#getConnectionNodes <em>Connection Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Connection Nodes</em>'.
	 * @see org.thirdeye.core.mesh.Mesh#getConnectionNodes()
	 * @see #getMesh()
	 * @generated
	 */
	EReference getMesh_ConnectionNodes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.thirdeye.core.mesh.Mesh#getProviderNodes <em>Provider Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Provider Nodes</em>'.
	 * @see org.thirdeye.core.mesh.Mesh#getProviderNodes()
	 * @see #getMesh()
	 * @generated
	 */
	EReference getMesh_ProviderNodes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.thirdeye.core.mesh.Mesh#getIntegratorEdges <em>Integrator Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Integrator Edges</em>'.
	 * @see org.thirdeye.core.mesh.Mesh#getIntegratorEdges()
	 * @see #getMesh()
	 * @generated
	 */
	EReference getMesh_IntegratorEdges();

	/**
	 * Returns the meta object for the containment reference list '{@link org.thirdeye.core.mesh.Mesh#getFromModelEdges <em>From Model Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>From Model Edges</em>'.
	 * @see org.thirdeye.core.mesh.Mesh#getFromModelEdges()
	 * @see #getMesh()
	 * @generated
	 */
	EReference getMesh_FromModelEdges();

	/**
	 * Returns the meta object for the containment reference list '{@link org.thirdeye.core.mesh.Mesh#getToProviderEdges <em>To Provider Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>To Provider Edges</em>'.
	 * @see org.thirdeye.core.mesh.Mesh#getToProviderEdges()
	 * @see #getMesh()
	 * @generated
	 */
	EReference getMesh_ToProviderEdges();

	/**
	 * Returns the meta object for the containment reference list '{@link org.thirdeye.core.mesh.Mesh#getFromConnectionEdges <em>From Connection Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>From Connection Edges</em>'.
	 * @see org.thirdeye.core.mesh.Mesh#getFromConnectionEdges()
	 * @see #getMesh()
	 * @generated
	 */
	EReference getMesh_FromConnectionEdges();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.core.mesh.Mesh#getEdges <em>Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Edges</em>'.
	 * @see org.thirdeye.core.mesh.Mesh#getEdges()
	 * @see #getMesh()
	 * @generated
	 */
	EAttribute getMesh_Edges();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.core.mesh.Mesh#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Nodes</em>'.
	 * @see org.thirdeye.core.mesh.Mesh#getNodes()
	 * @see #getMesh()
	 * @generated
	 */
	EAttribute getMesh_Nodes();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.mesh.Mesh#clone() <em>Clone</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clone</em>' operation.
	 * @see org.thirdeye.core.mesh.Mesh#clone()
	 * @generated
	 */
	EOperation getMesh__Clone();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.mesh.Mesh#print() <em>Print</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Print</em>' operation.
	 * @see org.thirdeye.core.mesh.Mesh#print()
	 * @generated
	 */
	EOperation getMesh__Print();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.mesh.Mesh#sync(org.thirdeye.core.mesh.Mesh) <em>Sync</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Sync</em>' operation.
	 * @see org.thirdeye.core.mesh.Mesh#sync(org.thirdeye.core.mesh.Mesh)
	 * @generated
	 */
	EOperation getMesh__Sync__Mesh();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MeshFactory getMeshFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.thirdeye.core.mesh.impl.NodeImpl <em>Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.mesh.impl.NodeImpl
		 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getNode()
		 * @generated
		 */
		EClass NODE = eINSTANCE.getNode();

		/**
		 * The meta object literal for the '<em><b>Variables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__VARIABLES = eINSTANCE.getNode_Variables();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE__NAME = eINSTANCE.getNode_Name();

		/**
		 * The meta object literal for the '<em><b>Run</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation NODE___RUN = eINSTANCE.getNode__Run();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation NODE___CALC = eINSTANCE.getNode__Calc();

		/**
		 * The meta object literal for the '<em><b>Logic</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation NODE___LOGIC = eINSTANCE.getNode__Logic();

		/**
		 * The meta object literal for the '{@link org.thirdeye.core.mesh.impl.VariableImpl <em>Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.mesh.impl.VariableImpl
		 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getVariable()
		 * @generated
		 */
		EClass VARIABLE = eINSTANCE.getVariable();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE__FEATURE = eINSTANCE.getVariable_Feature();

		/**
		 * The meta object literal for the '<em><b>Node</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE__NODE = eINSTANCE.getVariable_Node();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE__UNIT = eINSTANCE.getVariable_Unit();

		/**
		 * The meta object literal for the '<em><b>Accessors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE__ACCESSORS = eINSTANCE.getVariable_Accessors();

		/**
		 * The meta object literal for the '<em><b>Get</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation VARIABLE___GET__INT = eINSTANCE.getVariable__Get__int();

		/**
		 * The meta object literal for the '<em><b>Set</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation VARIABLE___SET__INT_OBJECT = eINSTANCE.getVariable__Set__int_Object();

		/**
		 * The meta object literal for the '{@link org.thirdeye.core.mesh.impl.ModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.mesh.impl.ModelImpl
		 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getModel()
		 * @generated
		 */
		EClass MODEL = eINSTANCE.getModel();

		/**
		 * The meta object literal for the '{@link org.thirdeye.core.mesh.impl.ConnectionImpl <em>Connection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.mesh.impl.ConnectionImpl
		 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getConnection()
		 * @generated
		 */
		EClass CONNECTION = eINSTANCE.getConnection();

		/**
		 * The meta object literal for the '{@link org.thirdeye.core.mesh.impl.ProviderImpl <em>Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.mesh.impl.ProviderImpl
		 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getProvider()
		 * @generated
		 */
		EClass PROVIDER = eINSTANCE.getProvider();

		/**
		 * The meta object literal for the '<em><b>Apply</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PROVIDER___APPLY__OBJECT = eINSTANCE.getProvider__Apply__Object();

		/**
		 * The meta object literal for the '{@link org.thirdeye.core.mesh.impl.EdgeImpl <em>Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.mesh.impl.EdgeImpl
		 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getEdge()
		 * @generated
		 */
		EClass EDGE = eINSTANCE.getEdge();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__FROM = eINSTANCE.getEdge_From();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__TO = eINSTANCE.getEdge_To();

		/**
		 * The meta object literal for the '<em><b>Run</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation EDGE___RUN = eINSTANCE.getEdge__Run();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation EDGE___CALC = eINSTANCE.getEdge__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.core.mesh.impl.TransformationEdgeImpl <em>Transformation Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.mesh.impl.TransformationEdgeImpl
		 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getTransformationEdge()
		 * @generated
		 */
		EClass TRANSFORMATION_EDGE = eINSTANCE.getTransformationEdge();

		/**
		 * The meta object literal for the '<em><b>Providers</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_EDGE__PROVIDERS = eINSTANCE.getTransformationEdge_Providers();

		/**
		 * The meta object literal for the '{@link org.thirdeye.core.mesh.impl.AccessorImpl <em>Accessor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.mesh.impl.AccessorImpl
		 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getAccessor()
		 * @generated
		 */
		EClass ACCESSOR = eINSTANCE.getAccessor();

		/**
		 * The meta object literal for the '<em><b>Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCESSOR__INDEX = eINSTANCE.getAccessor_Index();

		/**
		 * The meta object literal for the '<em><b>Var</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESSOR__VAR = eINSTANCE.getAccessor_Var();

		/**
		 * The meta object literal for the '{@link org.thirdeye.core.mesh.impl.MeshImpl <em>Mesh</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.mesh.impl.MeshImpl
		 * @see org.thirdeye.core.mesh.impl.MeshPackageImpl#getMesh()
		 * @generated
		 */
		EClass MESH = eINSTANCE.getMesh();

		/**
		 * The meta object literal for the '<em><b>Model Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESH__MODEL_NODES = eINSTANCE.getMesh_ModelNodes();

		/**
		 * The meta object literal for the '<em><b>Connection Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESH__CONNECTION_NODES = eINSTANCE.getMesh_ConnectionNodes();

		/**
		 * The meta object literal for the '<em><b>Provider Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESH__PROVIDER_NODES = eINSTANCE.getMesh_ProviderNodes();

		/**
		 * The meta object literal for the '<em><b>Integrator Edges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESH__INTEGRATOR_EDGES = eINSTANCE.getMesh_IntegratorEdges();

		/**
		 * The meta object literal for the '<em><b>From Model Edges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESH__FROM_MODEL_EDGES = eINSTANCE.getMesh_FromModelEdges();

		/**
		 * The meta object literal for the '<em><b>To Provider Edges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESH__TO_PROVIDER_EDGES = eINSTANCE.getMesh_ToProviderEdges();

		/**
		 * The meta object literal for the '<em><b>From Connection Edges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESH__FROM_CONNECTION_EDGES = eINSTANCE.getMesh_FromConnectionEdges();

		/**
		 * The meta object literal for the '<em><b>Edges</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESH__EDGES = eINSTANCE.getMesh_Edges();

		/**
		 * The meta object literal for the '<em><b>Nodes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESH__NODES = eINSTANCE.getMesh_Nodes();

		/**
		 * The meta object literal for the '<em><b>Clone</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESH___CLONE = eINSTANCE.getMesh__Clone();

		/**
		 * The meta object literal for the '<em><b>Print</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESH___PRINT = eINSTANCE.getMesh__Print();

		/**
		 * The meta object literal for the '<em><b>Sync</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESH___SYNC__MESH = eINSTANCE.getMesh__Sync__Mesh();

	}

} //MeshPackage
