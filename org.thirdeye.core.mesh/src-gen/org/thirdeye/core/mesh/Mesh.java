/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.mesh;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mesh</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.core.mesh.Mesh#getModelNodes <em>Model Nodes</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.Mesh#getConnectionNodes <em>Connection Nodes</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.Mesh#getProviderNodes <em>Provider Nodes</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.Mesh#getIntegratorEdges <em>Integrator Edges</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.Mesh#getFromModelEdges <em>From Model Edges</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.Mesh#getToProviderEdges <em>To Provider Edges</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.Mesh#getFromConnectionEdges <em>From Connection Edges</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.Mesh#getEdges <em>Edges</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.Mesh#getNodes <em>Nodes</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.core.mesh.MeshPackage#getMesh()
 * @model superTypes="org.thirdeye.core.dependencies.ERunnable"
 * @generated
 */
public interface Mesh extends EObject, Runnable {
	/**
	 * Returns the value of the '<em><b>Model Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link org.thirdeye.core.mesh.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Nodes</em>' containment reference list.
	 * @see org.thirdeye.core.mesh.MeshPackage#getMesh_ModelNodes()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="group='#nodes'"
	 * @generated
	 */
	EList<Node> getModelNodes();

	/**
	 * Returns the value of the '<em><b>Connection Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link org.thirdeye.core.mesh.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connection Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connection Nodes</em>' containment reference list.
	 * @see org.thirdeye.core.mesh.MeshPackage#getMesh_ConnectionNodes()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="group='#nodes'"
	 * @generated
	 */
	EList<Node> getConnectionNodes();

	/**
	 * Returns the value of the '<em><b>Provider Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link org.thirdeye.core.mesh.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provider Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provider Nodes</em>' containment reference list.
	 * @see org.thirdeye.core.mesh.MeshPackage#getMesh_ProviderNodes()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="group='#nodes'"
	 * @generated
	 */
	EList<Node> getProviderNodes();

	/**
	 * Returns the value of the '<em><b>Integrator Edges</b></em>' containment reference list.
	 * The list contents are of type {@link org.thirdeye.core.mesh.Edge}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integrator Edges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integrator Edges</em>' containment reference list.
	 * @see org.thirdeye.core.mesh.MeshPackage#getMesh_IntegratorEdges()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="group='#edges'"
	 * @generated
	 */
	EList<Edge> getIntegratorEdges();

	/**
	 * Returns the value of the '<em><b>From Model Edges</b></em>' containment reference list.
	 * The list contents are of type {@link org.thirdeye.core.mesh.Edge}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Model Edges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Model Edges</em>' containment reference list.
	 * @see org.thirdeye.core.mesh.MeshPackage#getMesh_FromModelEdges()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="group='#edges'"
	 * @generated
	 */
	EList<Edge> getFromModelEdges();

	/**
	 * Returns the value of the '<em><b>To Provider Edges</b></em>' containment reference list.
	 * The list contents are of type {@link org.thirdeye.core.mesh.Edge}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Provider Edges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Provider Edges</em>' containment reference list.
	 * @see org.thirdeye.core.mesh.MeshPackage#getMesh_ToProviderEdges()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="group='#edges'"
	 * @generated
	 */
	EList<Edge> getToProviderEdges();

	/**
	 * Returns the value of the '<em><b>From Connection Edges</b></em>' containment reference list.
	 * The list contents are of type {@link org.thirdeye.core.mesh.Edge}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Connection Edges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Connection Edges</em>' containment reference list.
	 * @see org.thirdeye.core.mesh.MeshPackage#getMesh_FromConnectionEdges()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="group='#edges'"
	 * @generated
	 */
	EList<Edge> getFromConnectionEdges();

	/**
	 * Returns the value of the '<em><b>Edges</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edges</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edges</em>' attribute list.
	 * @see org.thirdeye.core.mesh.MeshPackage#getMesh_Edges()
	 * @model dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group'"
	 * @generated
	 */
	FeatureMap getEdges();

	/**
	 * Returns the value of the '<em><b>Nodes</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nodes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nodes</em>' attribute list.
	 * @see org.thirdeye.core.mesh.MeshPackage#getMesh_Nodes()
	 * @model dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group'"
	 * @generated
	 */
	FeatureMap getNodes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 * @generated
	 */
	Mesh clone();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void print();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model otherRequired="true"
	 * @generated
	 */
	void sync(Mesh other);

} // Mesh
