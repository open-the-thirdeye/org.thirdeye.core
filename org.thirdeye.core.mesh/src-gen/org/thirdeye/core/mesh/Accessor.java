/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.mesh;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Accessor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.core.mesh.Accessor#getIndex <em>Index</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.Accessor#getVar <em>Var</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.core.mesh.MeshPackage#getAccessor()
 * @model
 * @generated
 */
public interface Accessor extends EObject {
	/**
	 * Returns the value of the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index</em>' attribute.
	 * @see #setIndex(int)
	 * @see org.thirdeye.core.mesh.MeshPackage#getAccessor_Index()
	 * @model required="true"
	 * @generated
	 */
	int getIndex();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.mesh.Accessor#getIndex <em>Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index</em>' attribute.
	 * @see #getIndex()
	 * @generated
	 */
	void setIndex(int value);

	/**
	 * Returns the value of the '<em><b>Var</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.thirdeye.core.mesh.Variable#getAccessors <em>Accessors</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var</em>' container reference.
	 * @see org.thirdeye.core.mesh.MeshPackage#getAccessor_Var()
	 * @see org.thirdeye.core.mesh.Variable#getAccessors
	 * @model opposite="accessors" required="true" changeable="false" derived="true"
	 * @generated
	 */
	Variable getVar();

} // Accessor
