/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.mesh.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap.Entry;
import org.eclipse.emf.ecore.util.InternalEList;
import org.thirdeye.core.mesh.Accessor;
import org.thirdeye.core.mesh.Edge;
import org.thirdeye.core.mesh.Mesh;
import org.thirdeye.core.mesh.MeshPackage;
import org.thirdeye.core.mesh.Node;
import org.thirdeye.core.mesh.Variable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mesh</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.core.mesh.impl.MeshImpl#getModelNodes <em>Model Nodes</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.impl.MeshImpl#getConnectionNodes <em>Connection Nodes</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.impl.MeshImpl#getProviderNodes <em>Provider Nodes</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.impl.MeshImpl#getIntegratorEdges <em>Integrator Edges</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.impl.MeshImpl#getFromModelEdges <em>From Model Edges</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.impl.MeshImpl#getToProviderEdges <em>To Provider Edges</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.impl.MeshImpl#getFromConnectionEdges <em>From Connection Edges</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.impl.MeshImpl#getEdges <em>Edges</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.impl.MeshImpl#getNodes <em>Nodes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MeshImpl extends MinimalEObjectImpl.Container implements Mesh {
	/**
	 * The cached value of the '{@link #getEdges() <em>Edges</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdges()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap edges;

	/**
	 * The cached value of the '{@link #getNodes() <em>Nodes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodes()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap nodes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MeshImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MeshPackage.Literals.MESH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Node> getModelNodes() {
		return getNodes().list(MeshPackage.Literals.MESH__MODEL_NODES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Node> getConnectionNodes() {
		return getNodes().list(MeshPackage.Literals.MESH__CONNECTION_NODES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Node> getProviderNodes() {
		return getNodes().list(MeshPackage.Literals.MESH__PROVIDER_NODES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Edge> getIntegratorEdges() {
		return getEdges().list(MeshPackage.Literals.MESH__INTEGRATOR_EDGES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Edge> getFromModelEdges() {
		return getEdges().list(MeshPackage.Literals.MESH__FROM_MODEL_EDGES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Edge> getToProviderEdges() {
		return getEdges().list(MeshPackage.Literals.MESH__TO_PROVIDER_EDGES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Edge> getFromConnectionEdges() {
		return getEdges().list(MeshPackage.Literals.MESH__FROM_CONNECTION_EDGES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getEdges() {
		if (edges == null) {
			edges = new BasicFeatureMap(this, MeshPackage.MESH__EDGES);
		}
		return edges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getNodes() {
		if (nodes == null) {
			nodes = new BasicFeatureMap(this, MeshPackage.MESH__NODES);
		}
		return nodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Mesh clone() {
		
		Copier copier = new Copier();
		Mesh copyConf = (Mesh) copier.copy(this);
		copier.copyReferences();
		
		for( Node node: copyConf.getModelNodes()) {
			node.getVariables();
		}
		for( Node node: copyConf.getProviderNodes()) {
			node.getVariables();
		}
		for( Node node: copyConf.getConnectionNodes()) {
			node.getVariables();
		}
		
		for( Entry con: copyConf.getEdges() ) {
			Edge edge = (Edge) con.getValue();
			Accessor newAccess = edge.getFrom();
			
			for( Variable oldVar : ((Node)copier.get(newAccess.getVar().getNode())).getVariables() ) {
				for( Accessor oldAccess : oldVar.getAccessors() ) {
					if( oldAccess.getVar().getFeature().equals( newAccess.getVar().getFeature() ) &&
					    ( oldAccess.getIndex() == newAccess.getIndex() ) ) {
						edge.setFrom(oldAccess);
					}			
				}
			}
			int index = 0;
			for( Accessor newerAccess: edge.getTo() ) {
				for( Variable oldVar : ((Node)copier.get(newerAccess.getVar().getNode())).getVariables() ) {
					for( Accessor oldAccess : oldVar.getAccessors() ) {
						if( oldAccess.getVar().getFeature().equals( newerAccess.getVar().getFeature() ) &&
						    ( oldAccess.getIndex() == newerAccess.getIndex() ) ) {
							edge.getTo().set(index,oldAccess);
							index++;
						}
					}

				}
			}
			
		}
		return copyConf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void print() {
		System.out.println("Mesh:");
		System.out.println();
		System.out.println("num of models:                " +getModelNodes().size());
		System.out.println("num of providers:             " +getProviderNodes().size());
		System.out.println("num of connections:           " +getConnectionNodes().size());
		System.out.println("----------------------------------------------------------------------------");
		System.out.println("sum of nodes:                 " +getNodes().size());
		System.out.println();
		System.out.println("num of integrator edges:      " +getIntegratorEdges().size());
		System.out.println("num of from model edges:      " +getFromModelEdges().size());
		System.out.println("num of from connection edges: " +getFromConnectionEdges().size());
		System.out.println("num of to provider edges:     " +getToProviderEdges().size());
		System.out.println("----------------------------------------------------------------------------");
		System.out.println("sum of edges:                 " +getEdges().size());
		System.out.println("============================================================================");
		for( Node node: getModelNodes() ) {
			System.out.println(node.toString());
		}
		System.out.println("============================================================================");
		for( Node node: getProviderNodes() ) {
			System.out.println(node.toString());
		}
		System.out.println("============================================================================");
		for( Node node: getConnectionNodes() ) {
			System.out.println(node.toString());
		}
		System.out.println("============================================================================");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void sync(Mesh other) {
		int i = 0;
		for( Node node: getModelNodes()) {
			Node otherNode = other.getModelNodes().get(i);
			int j = 0;
			for( Variable var: node.getVariables() ) {
				Variable otherVar = otherNode.getVariables().get(j);
				var.set(-1, otherVar.get(-1));
				j++;
			}
			i++;
		}
		i = 0;
		for( Node node: getProviderNodes()) {
			Node otherNode = other.getModelNodes().get(i);
			int j = 0;
			for( Variable var: node.getVariables() ) {
				Variable otherVar = otherNode.getVariables().get(j);
				var.set(-1, otherVar.get(-1));
				j++;
			}
			i++;
		}
		i = 0;
		for( Node node: getConnectionNodes()) {
			Node otherNode = other.getModelNodes().get(i);
			int j = 0;
			for( Variable var: node.getVariables() ) {
				Variable otherVar = otherNode.getVariables().get(j);
				var.set(-1, otherVar.get(-1));
				j++;
			}
			i++;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MeshPackage.MESH__MODEL_NODES:
				return ((InternalEList<?>)getModelNodes()).basicRemove(otherEnd, msgs);
			case MeshPackage.MESH__CONNECTION_NODES:
				return ((InternalEList<?>)getConnectionNodes()).basicRemove(otherEnd, msgs);
			case MeshPackage.MESH__PROVIDER_NODES:
				return ((InternalEList<?>)getProviderNodes()).basicRemove(otherEnd, msgs);
			case MeshPackage.MESH__INTEGRATOR_EDGES:
				return ((InternalEList<?>)getIntegratorEdges()).basicRemove(otherEnd, msgs);
			case MeshPackage.MESH__FROM_MODEL_EDGES:
				return ((InternalEList<?>)getFromModelEdges()).basicRemove(otherEnd, msgs);
			case MeshPackage.MESH__TO_PROVIDER_EDGES:
				return ((InternalEList<?>)getToProviderEdges()).basicRemove(otherEnd, msgs);
			case MeshPackage.MESH__FROM_CONNECTION_EDGES:
				return ((InternalEList<?>)getFromConnectionEdges()).basicRemove(otherEnd, msgs);
			case MeshPackage.MESH__EDGES:
				return ((InternalEList<?>)getEdges()).basicRemove(otherEnd, msgs);
			case MeshPackage.MESH__NODES:
				return ((InternalEList<?>)getNodes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MeshPackage.MESH__MODEL_NODES:
				return getModelNodes();
			case MeshPackage.MESH__CONNECTION_NODES:
				return getConnectionNodes();
			case MeshPackage.MESH__PROVIDER_NODES:
				return getProviderNodes();
			case MeshPackage.MESH__INTEGRATOR_EDGES:
				return getIntegratorEdges();
			case MeshPackage.MESH__FROM_MODEL_EDGES:
				return getFromModelEdges();
			case MeshPackage.MESH__TO_PROVIDER_EDGES:
				return getToProviderEdges();
			case MeshPackage.MESH__FROM_CONNECTION_EDGES:
				return getFromConnectionEdges();
			case MeshPackage.MESH__EDGES:
				if (coreType) return getEdges();
				return ((FeatureMap.Internal)getEdges()).getWrapper();
			case MeshPackage.MESH__NODES:
				if (coreType) return getNodes();
				return ((FeatureMap.Internal)getNodes()).getWrapper();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MeshPackage.MESH__MODEL_NODES:
				getModelNodes().clear();
				getModelNodes().addAll((Collection<? extends Node>)newValue);
				return;
			case MeshPackage.MESH__CONNECTION_NODES:
				getConnectionNodes().clear();
				getConnectionNodes().addAll((Collection<? extends Node>)newValue);
				return;
			case MeshPackage.MESH__PROVIDER_NODES:
				getProviderNodes().clear();
				getProviderNodes().addAll((Collection<? extends Node>)newValue);
				return;
			case MeshPackage.MESH__INTEGRATOR_EDGES:
				getIntegratorEdges().clear();
				getIntegratorEdges().addAll((Collection<? extends Edge>)newValue);
				return;
			case MeshPackage.MESH__FROM_MODEL_EDGES:
				getFromModelEdges().clear();
				getFromModelEdges().addAll((Collection<? extends Edge>)newValue);
				return;
			case MeshPackage.MESH__TO_PROVIDER_EDGES:
				getToProviderEdges().clear();
				getToProviderEdges().addAll((Collection<? extends Edge>)newValue);
				return;
			case MeshPackage.MESH__FROM_CONNECTION_EDGES:
				getFromConnectionEdges().clear();
				getFromConnectionEdges().addAll((Collection<? extends Edge>)newValue);
				return;
			case MeshPackage.MESH__EDGES:
				((FeatureMap.Internal)getEdges()).set(newValue);
				return;
			case MeshPackage.MESH__NODES:
				((FeatureMap.Internal)getNodes()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MeshPackage.MESH__MODEL_NODES:
				getModelNodes().clear();
				return;
			case MeshPackage.MESH__CONNECTION_NODES:
				getConnectionNodes().clear();
				return;
			case MeshPackage.MESH__PROVIDER_NODES:
				getProviderNodes().clear();
				return;
			case MeshPackage.MESH__INTEGRATOR_EDGES:
				getIntegratorEdges().clear();
				return;
			case MeshPackage.MESH__FROM_MODEL_EDGES:
				getFromModelEdges().clear();
				return;
			case MeshPackage.MESH__TO_PROVIDER_EDGES:
				getToProviderEdges().clear();
				return;
			case MeshPackage.MESH__FROM_CONNECTION_EDGES:
				getFromConnectionEdges().clear();
				return;
			case MeshPackage.MESH__EDGES:
				getEdges().clear();
				return;
			case MeshPackage.MESH__NODES:
				getNodes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MeshPackage.MESH__MODEL_NODES:
				return !getModelNodes().isEmpty();
			case MeshPackage.MESH__CONNECTION_NODES:
				return !getConnectionNodes().isEmpty();
			case MeshPackage.MESH__PROVIDER_NODES:
				return !getProviderNodes().isEmpty();
			case MeshPackage.MESH__INTEGRATOR_EDGES:
				return !getIntegratorEdges().isEmpty();
			case MeshPackage.MESH__FROM_MODEL_EDGES:
				return !getFromModelEdges().isEmpty();
			case MeshPackage.MESH__TO_PROVIDER_EDGES:
				return !getToProviderEdges().isEmpty();
			case MeshPackage.MESH__FROM_CONNECTION_EDGES:
				return !getFromConnectionEdges().isEmpty();
			case MeshPackage.MESH__EDGES:
				return edges != null && !edges.isEmpty();
			case MeshPackage.MESH__NODES:
				return nodes != null && !nodes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case MeshPackage.MESH___CLONE:
				return clone();
			case MeshPackage.MESH___PRINT:
				print();
				return null;
			case MeshPackage.MESH___SYNC__MESH:
				sync((Mesh)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (edges: ");
		result.append(edges);
		result.append(", nodes: ");
		result.append(nodes);
		result.append(')');
		return result.toString();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

} //MeshImpl
