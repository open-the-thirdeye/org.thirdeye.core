/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.mesh.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.thirdeye.core.mesh.Accessor;
import org.thirdeye.core.mesh.MeshFactory;
import org.thirdeye.core.mesh.MeshPackage;
import org.thirdeye.core.mesh.Node;
import org.thirdeye.core.mesh.Variable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.core.mesh.impl.VariableImpl#getFeature <em>Feature</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.impl.VariableImpl#getNode <em>Node</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.impl.VariableImpl#getUnit <em>Unit</em>}</li>
 *   <li>{@link org.thirdeye.core.mesh.impl.VariableImpl#getAccessors <em>Accessors</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VariableImpl extends MinimalEObjectImpl.Container implements Variable {
	/**
	 * The default value of the '{@link #getFeature() <em>Feature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeature()
	 * @generated
	 * @ordered
	 */
	protected static final String FEATURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFeature() <em>Feature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeature()
	 * @generated
	 * @ordered
	 */
	protected String feature = FEATURE_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnit() <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnit()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnit() <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnit()
	 * @generated
	 * @ordered
	 */
	protected String unit = UNIT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAccessors() <em>Accessors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessors()
	 * @generated
	 * @ordered
	 */
	protected EList<Accessor> accessors;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MeshPackage.Literals.VARIABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFeature() {
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFeature(String newFeature) {
		String oldFeature = feature;
		feature = newFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MeshPackage.VARIABLE__FEATURE, oldFeature, feature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Node getNode() {
		if (eContainerFeatureID() != MeshPackage.VARIABLE__NODE) return null;
		return (Node)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getUnit() {
		return unit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUnit(String newUnit) {
		String oldUnit = unit;
		unit = newUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MeshPackage.VARIABLE__UNIT, oldUnit, unit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Accessor> getAccessorsGen() {
		if (accessors == null) {
			accessors = new EObjectContainmentWithInverseEList<Accessor>(Accessor.class, this, MeshPackage.VARIABLE__ACCESSORS, MeshPackage.ACCESSOR__VAR);
		}
		return accessors;
	}
	
	/**
	 * <!-- begin-user-doc -->
 	 * Automatic creation of variables.
 	 * <!-- end-user-doc -->
	 * @generated NOT (Patch)
	 */
	@SuppressWarnings("rawtypes")
	public EList<Accessor> getAccessors() {
		MeshPackageImpl.init();
		MeshFactory factory = MeshFactory.eINSTANCE;

		EList<Accessor> temp = getAccessorsGen();
		
		if (temp.size()==0) {
			Object obj = get(-1);
			Accessor access = factory.createAccessor();
			access.setIndex(-1);
			temp.add(access);
			if( obj instanceof EList ) {
				for( int i = 0; i < ((EList)obj).size(); i++) {
					access = factory.createAccessor();
					access.setIndex(i);
					temp.add(access);
				}
				
			}
		}
		return temp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Object get(final int index) {
		EStructuralFeature tempFeature = getNode().eClass().getEStructuralFeature(getFeature());
		Object o = getNode().eGet(tempFeature);
		if (index < -1) {
			return 0;
			//TODO Handle Error
			//ERROR
		}
		else if (index == -1) {
			return o;
		}
		else {
			EList<?> tempList = (EList<?>) o;
			if (index < tempList.size()) {
				return tempList.get(index);
			}
			else {
				return 0;
				//TODO Handle Error
				//ERROR
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void set(final int index, final Object value) {
		EStructuralFeature tempFeature = getNode().eClass().getEStructuralFeature(getFeature());
		if (index < -1) {
			//TODO Handle Error
			//ERROR
		} else if (index == -1) {
			if( value instanceof Object[] ) {
				EList<Double> tempD = new BasicEList<Double>();
				for( int i = 0; i< ((Double[])value).length; i++ ) {
					tempD.add(((Double[])value)[i]);
				}
				getNode().eSet(tempFeature, tempD);
			} else {
				getNode().eSet(tempFeature, value);				
			}
		} else {
			@SuppressWarnings("unchecked") EList<Object> tempList = (EList<Object>) getNode().eGet(tempFeature);
			if (index < tempList.size()) {
				tempList.set(index, value);
			} else {
				//TODO Handle Error
				//ERROR
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MeshPackage.VARIABLE__NODE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return eBasicSetContainer(otherEnd, MeshPackage.VARIABLE__NODE, msgs);
			case MeshPackage.VARIABLE__ACCESSORS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAccessors()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MeshPackage.VARIABLE__NODE:
				return eBasicSetContainer(null, MeshPackage.VARIABLE__NODE, msgs);
			case MeshPackage.VARIABLE__ACCESSORS:
				return ((InternalEList<?>)getAccessors()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case MeshPackage.VARIABLE__NODE:
				return eInternalContainer().eInverseRemove(this, MeshPackage.NODE__VARIABLES, Node.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MeshPackage.VARIABLE__FEATURE:
				return getFeature();
			case MeshPackage.VARIABLE__NODE:
				return getNode();
			case MeshPackage.VARIABLE__UNIT:
				return getUnit();
			case MeshPackage.VARIABLE__ACCESSORS:
				return getAccessors();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MeshPackage.VARIABLE__FEATURE:
				setFeature((String)newValue);
				return;
			case MeshPackage.VARIABLE__UNIT:
				setUnit((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MeshPackage.VARIABLE__FEATURE:
				setFeature(FEATURE_EDEFAULT);
				return;
			case MeshPackage.VARIABLE__UNIT:
				setUnit(UNIT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MeshPackage.VARIABLE__FEATURE:
				return FEATURE_EDEFAULT == null ? feature != null : !FEATURE_EDEFAULT.equals(feature);
			case MeshPackage.VARIABLE__NODE:
				return getNode() != null;
			case MeshPackage.VARIABLE__UNIT:
				return UNIT_EDEFAULT == null ? unit != null : !UNIT_EDEFAULT.equals(unit);
			case MeshPackage.VARIABLE__ACCESSORS:
				return accessors != null && !accessors.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case MeshPackage.VARIABLE___GET__INT:
				return get((Integer)arguments.get(0));
			case MeshPackage.VARIABLE___SET__INT_OBJECT:
				set((Integer)arguments.get(0), arguments.get(1));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (feature: ");
		result.append(feature);
		result.append(", unit: ");
		result.append(unit);
		result.append(')');
		return result.toString();
	}

} //VariableImpl
