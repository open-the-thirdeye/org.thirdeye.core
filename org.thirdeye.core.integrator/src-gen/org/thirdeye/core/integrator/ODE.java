/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.integrator;

import org.thirdeye.core.mesh.TransformationEdge;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ODE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.thirdeye.core.integrator.ODE#getIntegrator <em>Integrator</em>}</li>
 *   <li>{@link org.thirdeye.core.integrator.ODE#getEqIndex <em>Eq Index</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.thirdeye.core.integrator.IntegratorPackage#getODE()
 * @model
 * @generated
 */
public interface ODE extends TransformationEdge {
	/**
	 * Returns the value of the '<em><b>Integrator</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.thirdeye.core.integrator.Integrator#getOdeCollection <em>Ode Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integrator</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integrator</em>' reference.
	 * @see #setIntegrator(Integrator)
	 * @see org.thirdeye.core.integrator.IntegratorPackage#getODE_Integrator()
	 * @see org.thirdeye.core.integrator.Integrator#getOdeCollection
	 * @model opposite="odeCollection" required="true"
	 * @generated
	 */
	Integrator getIntegrator();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.integrator.ODE#getIntegrator <em>Integrator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integrator</em>' reference.
	 * @see #getIntegrator()
	 * @generated
	 */
	void setIntegrator(Integrator value);

	/**
	 * Returns the value of the '<em><b>Eq Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Eq Index</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Eq Index</em>' attribute.
	 * @see #setEqIndex(int)
	 * @see org.thirdeye.core.integrator.IntegratorPackage#getODE_EqIndex()
	 * @model required="true"
	 * @generated
	 */
	int getEqIndex();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.integrator.ODE#getEqIndex <em>Eq Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Eq Index</em>' attribute.
	 * @see #getEqIndex()
	 * @generated
	 */
	void setEqIndex(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='getIntegrator().getEquationSystem().getY()[getEqIndex()] = (double)getFrom().get(getFromIndex());'"
	 * @generated
	 */
	void getDerivativeFromVar();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='getTo().set(getToIndex(),getIntegrator().getEquationSystem().getY()[getEqIndex()]);'"
	 * @generated
	 */
	void setIntegralToVar();

} // ODE
