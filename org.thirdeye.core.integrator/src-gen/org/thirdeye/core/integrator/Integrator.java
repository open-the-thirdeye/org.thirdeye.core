/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.integrator;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integrator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.thirdeye.core.integrator.Integrator#getEquationSystem <em>Equation System</em>}</li>
 *   <li>{@link org.thirdeye.core.integrator.Integrator#getOdeCollection <em>Ode Collection</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.thirdeye.core.integrator.IntegratorPackage#getIntegrator()
 * @model abstract="true" superTypes="org.thirdeye.core.dependencies.ERunnable"
 * @generated
 */
public interface Integrator extends EObject, Runnable {
	/**
	 * Returns the value of the '<em><b>Equation System</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equation System</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equation System</em>' containment reference.
	 * @see org.thirdeye.core.integrator.IntegratorPackage#getIntegrator_EquationSystem()
	 * @model containment="true" required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EquationSystem getEquationSystem();

	/**
	 * Returns the value of the '<em><b>Ode Collection</b></em>' reference list.
	 * The list contents are of type {@link org.thirdeye.core.integrator.ODE}.
	 * It is bidirectional and its opposite is '{@link org.thirdeye.core.integrator.ODE#getIntegrator <em>Integrator</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ode Collection</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ode Collection</em>' reference list.
	 * @see org.thirdeye.core.integrator.IntegratorPackage#getIntegrator_OdeCollection()
	 * @see org.thirdeye.core.integrator.ODE#getIntegrator
	 * @model opposite="integrator"
	 * @generated
	 */
	EList<ODE> getOdeCollection();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void run();

} // Integrator
