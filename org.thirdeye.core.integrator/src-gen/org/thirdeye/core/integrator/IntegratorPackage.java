/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.integrator;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.thirdeye.core.dependencies.DependenciesPackage;
import org.thirdeye.core.mesh.MeshPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.thirdeye.core.integrator.IntegratorFactory
 * @model kind="package"
 * @generated
 */
public interface IntegratorPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "integrator";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://integrator/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "integrator";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	IntegratorPackage eINSTANCE = org.thirdeye.core.integrator.impl.IntegratorPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.thirdeye.core.integrator.impl.ODEImpl <em>ODE</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.integrator.impl.ODEImpl
	 * @see org.thirdeye.core.integrator.impl.IntegratorPackageImpl#getODE()
	 * @generated
	 */
	int ODE = 0;

	/**
	 * The feature id for the '<em><b>From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ODE__FROM = MeshPackage.TRANSFORMATION_EDGE__FROM;

	/**
	 * The feature id for the '<em><b>To</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ODE__TO = MeshPackage.TRANSFORMATION_EDGE__TO;

	/**
	 * The feature id for the '<em><b>Providers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ODE__PROVIDERS = MeshPackage.TRANSFORMATION_EDGE__PROVIDERS;

	/**
	 * The feature id for the '<em><b>Integrator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ODE__INTEGRATOR = MeshPackage.TRANSFORMATION_EDGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Eq Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ODE__EQ_INDEX = MeshPackage.TRANSFORMATION_EDGE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>ODE</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ODE_FEATURE_COUNT = MeshPackage.TRANSFORMATION_EDGE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ODE___RUN = MeshPackage.TRANSFORMATION_EDGE___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ODE___CALC = MeshPackage.TRANSFORMATION_EDGE___CALC;

	/**
	 * The operation id for the '<em>Get Derivative From Var</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ODE___GET_DERIVATIVE_FROM_VAR = MeshPackage.TRANSFORMATION_EDGE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Set Integral To Var</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ODE___SET_INTEGRAL_TO_VAR = MeshPackage.TRANSFORMATION_EDGE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>ODE</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ODE_OPERATION_COUNT = MeshPackage.TRANSFORMATION_EDGE_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.thirdeye.core.integrator.impl.ApacheIntegratorImpl <em>Apache Integrator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.integrator.impl.ApacheIntegratorImpl
	 * @see org.thirdeye.core.integrator.impl.IntegratorPackageImpl#getApacheIntegrator()
	 * @generated
	 */
	int APACHE_INTEGRATOR = 1;

	/**
	 * The feature id for the '<em><b>Equation System</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACHE_INTEGRATOR__EQUATION_SYSTEM = DependenciesPackage.EFIRST_ORDER_DIFFERENTIAL_EQUATIONS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Ode Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACHE_INTEGRATOR__ODE_COLLECTION = DependenciesPackage.EFIRST_ORDER_DIFFERENTIAL_EQUATIONS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Barrier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACHE_INTEGRATOR__BARRIER = DependenciesPackage.EFIRST_ORDER_DIFFERENTIAL_EQUATIONS_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Integrator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACHE_INTEGRATOR__INTEGRATOR = DependenciesPackage.EFIRST_ORDER_DIFFERENTIAL_EQUATIONS_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Apache Integrator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACHE_INTEGRATOR_FEATURE_COUNT = DependenciesPackage.EFIRST_ORDER_DIFFERENTIAL_EQUATIONS_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACHE_INTEGRATOR___RUN = DependenciesPackage.EFIRST_ORDER_DIFFERENTIAL_EQUATIONS_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Dimension</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACHE_INTEGRATOR___GET_DIMENSION = DependenciesPackage.EFIRST_ORDER_DIFFERENTIAL_EQUATIONS_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Compute Derivatives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACHE_INTEGRATOR___COMPUTE_DERIVATIVES__DOUBLE_DOUBLE_DOUBLE = DependenciesPackage.EFIRST_ORDER_DIFFERENTIAL_EQUATIONS_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Apache Integrator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACHE_INTEGRATOR_OPERATION_COUNT = DependenciesPackage.EFIRST_ORDER_DIFFERENTIAL_EQUATIONS_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.thirdeye.core.integrator.impl.IntegratorImpl <em>Integrator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.integrator.impl.IntegratorImpl
	 * @see org.thirdeye.core.integrator.impl.IntegratorPackageImpl#getIntegrator()
	 * @generated
	 */
	int INTEGRATOR = 2;

	/**
	 * The feature id for the '<em><b>Equation System</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGRATOR__EQUATION_SYSTEM = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Ode Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGRATOR__ODE_COLLECTION = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Integrator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGRATOR_FEATURE_COUNT = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGRATOR___RUN = DependenciesPackage.ERUNNABLE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Integrator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGRATOR_OPERATION_COUNT = DependenciesPackage.ERUNNABLE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.core.integrator.impl.EquationSystemImpl <em>Equation System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.integrator.impl.EquationSystemImpl
	 * @see org.thirdeye.core.integrator.impl.IntegratorPackageImpl#getEquationSystem()
	 * @generated
	 */
	int EQUATION_SYSTEM = 3;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUATION_SYSTEM__Y = 0;

	/**
	 * The feature id for the '<em><b>YDot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUATION_SYSTEM__YDOT = 1;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUATION_SYSTEM__DIMENSION = 2;

	/**
	 * The number of structural features of the '<em>Equation System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUATION_SYSTEM_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Equation System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUATION_SYSTEM_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.integrator.ODE <em>ODE</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ODE</em>'.
	 * @see org.thirdeye.core.integrator.ODE
	 * @generated
	 */
	EClass getODE();

	/**
	 * Returns the meta object for the reference '{@link org.thirdeye.core.integrator.ODE#getIntegrator <em>Integrator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Integrator</em>'.
	 * @see org.thirdeye.core.integrator.ODE#getIntegrator()
	 * @see #getODE()
	 * @generated
	 */
	EReference getODE_Integrator();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.integrator.ODE#getEqIndex <em>Eq Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Eq Index</em>'.
	 * @see org.thirdeye.core.integrator.ODE#getEqIndex()
	 * @see #getODE()
	 * @generated
	 */
	EAttribute getODE_EqIndex();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.integrator.ODE#getDerivativeFromVar() <em>Get Derivative From Var</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Derivative From Var</em>' operation.
	 * @see org.thirdeye.core.integrator.ODE#getDerivativeFromVar()
	 * @generated
	 */
	EOperation getODE__GetDerivativeFromVar();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.integrator.ODE#setIntegralToVar() <em>Set Integral To Var</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Integral To Var</em>' operation.
	 * @see org.thirdeye.core.integrator.ODE#setIntegralToVar()
	 * @generated
	 */
	EOperation getODE__SetIntegralToVar();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.integrator.ApacheIntegrator <em>Apache Integrator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Apache Integrator</em>'.
	 * @see org.thirdeye.core.integrator.ApacheIntegrator
	 * @generated
	 */
	EClass getApacheIntegrator();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.integrator.ApacheIntegrator#getBarrier <em>Barrier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Barrier</em>'.
	 * @see org.thirdeye.core.integrator.ApacheIntegrator#getBarrier()
	 * @see #getApacheIntegrator()
	 * @generated
	 */
	EAttribute getApacheIntegrator_Barrier();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.integrator.ApacheIntegrator#getIntegrator <em>Integrator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integrator</em>'.
	 * @see org.thirdeye.core.integrator.ApacheIntegrator#getIntegrator()
	 * @see #getApacheIntegrator()
	 * @generated
	 */
	EAttribute getApacheIntegrator_Integrator();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.integrator.ApacheIntegrator#run() <em>Run</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Run</em>' operation.
	 * @see org.thirdeye.core.integrator.ApacheIntegrator#run()
	 * @generated
	 */
	EOperation getApacheIntegrator__Run();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.integrator.ApacheIntegrator#getDimension() <em>Get Dimension</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Dimension</em>' operation.
	 * @see org.thirdeye.core.integrator.ApacheIntegrator#getDimension()
	 * @generated
	 */
	EOperation getApacheIntegrator__GetDimension();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.integrator.ApacheIntegrator#computeDerivatives(double, double[], double[]) <em>Compute Derivatives</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Compute Derivatives</em>' operation.
	 * @see org.thirdeye.core.integrator.ApacheIntegrator#computeDerivatives(double, double[], double[])
	 * @generated
	 */
	EOperation getApacheIntegrator__ComputeDerivatives__double_double_double();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.integrator.Integrator <em>Integrator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integrator</em>'.
	 * @see org.thirdeye.core.integrator.Integrator
	 * @generated
	 */
	EClass getIntegrator();

	/**
	 * Returns the meta object for the containment reference '{@link org.thirdeye.core.integrator.Integrator#getEquationSystem <em>Equation System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Equation System</em>'.
	 * @see org.thirdeye.core.integrator.Integrator#getEquationSystem()
	 * @see #getIntegrator()
	 * @generated
	 */
	EReference getIntegrator_EquationSystem();

	/**
	 * Returns the meta object for the reference list '{@link org.thirdeye.core.integrator.Integrator#getOdeCollection <em>Ode Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ode Collection</em>'.
	 * @see org.thirdeye.core.integrator.Integrator#getOdeCollection()
	 * @see #getIntegrator()
	 * @generated
	 */
	EReference getIntegrator_OdeCollection();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.integrator.Integrator#run() <em>Run</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Run</em>' operation.
	 * @see org.thirdeye.core.integrator.Integrator#run()
	 * @generated
	 */
	EOperation getIntegrator__Run();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.integrator.EquationSystem <em>Equation System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equation System</em>'.
	 * @see org.thirdeye.core.integrator.EquationSystem
	 * @generated
	 */
	EClass getEquationSystem();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.integrator.EquationSystem#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see org.thirdeye.core.integrator.EquationSystem#getY()
	 * @see #getEquationSystem()
	 * @generated
	 */
	EAttribute getEquationSystem_Y();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.integrator.EquationSystem#getYDot <em>YDot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>YDot</em>'.
	 * @see org.thirdeye.core.integrator.EquationSystem#getYDot()
	 * @see #getEquationSystem()
	 * @generated
	 */
	EAttribute getEquationSystem_YDot();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.integrator.EquationSystem#getDimension <em>Dimension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dimension</em>'.
	 * @see org.thirdeye.core.integrator.EquationSystem#getDimension()
	 * @see #getEquationSystem()
	 * @generated
	 */
	EAttribute getEquationSystem_Dimension();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	IntegratorFactory getIntegratorFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.thirdeye.core.integrator.impl.ODEImpl <em>ODE</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.integrator.impl.ODEImpl
		 * @see org.thirdeye.core.integrator.impl.IntegratorPackageImpl#getODE()
		 * @generated
		 */
		EClass ODE = eINSTANCE.getODE();

		/**
		 * The meta object literal for the '<em><b>Integrator</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ODE__INTEGRATOR = eINSTANCE.getODE_Integrator();

		/**
		 * The meta object literal for the '<em><b>Eq Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ODE__EQ_INDEX = eINSTANCE.getODE_EqIndex();

		/**
		 * The meta object literal for the '<em><b>Get Derivative From Var</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ODE___GET_DERIVATIVE_FROM_VAR = eINSTANCE.getODE__GetDerivativeFromVar();

		/**
		 * The meta object literal for the '<em><b>Set Integral To Var</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ODE___SET_INTEGRAL_TO_VAR = eINSTANCE.getODE__SetIntegralToVar();

		/**
		 * The meta object literal for the '{@link org.thirdeye.core.integrator.impl.ApacheIntegratorImpl <em>Apache Integrator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.integrator.impl.ApacheIntegratorImpl
		 * @see org.thirdeye.core.integrator.impl.IntegratorPackageImpl#getApacheIntegrator()
		 * @generated
		 */
		EClass APACHE_INTEGRATOR = eINSTANCE.getApacheIntegrator();

		/**
		 * The meta object literal for the '<em><b>Barrier</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APACHE_INTEGRATOR__BARRIER = eINSTANCE.getApacheIntegrator_Barrier();

		/**
		 * The meta object literal for the '<em><b>Integrator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APACHE_INTEGRATOR__INTEGRATOR = eINSTANCE.getApacheIntegrator_Integrator();

		/**
		 * The meta object literal for the '<em><b>Run</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APACHE_INTEGRATOR___RUN = eINSTANCE.getApacheIntegrator__Run();

		/**
		 * The meta object literal for the '<em><b>Get Dimension</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APACHE_INTEGRATOR___GET_DIMENSION = eINSTANCE.getApacheIntegrator__GetDimension();

		/**
		 * The meta object literal for the '<em><b>Compute Derivatives</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APACHE_INTEGRATOR___COMPUTE_DERIVATIVES__DOUBLE_DOUBLE_DOUBLE = eINSTANCE.getApacheIntegrator__ComputeDerivatives__double_double_double();

		/**
		 * The meta object literal for the '{@link org.thirdeye.core.integrator.impl.IntegratorImpl <em>Integrator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.integrator.impl.IntegratorImpl
		 * @see org.thirdeye.core.integrator.impl.IntegratorPackageImpl#getIntegrator()
		 * @generated
		 */
		EClass INTEGRATOR = eINSTANCE.getIntegrator();

		/**
		 * The meta object literal for the '<em><b>Equation System</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTEGRATOR__EQUATION_SYSTEM = eINSTANCE.getIntegrator_EquationSystem();

		/**
		 * The meta object literal for the '<em><b>Ode Collection</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTEGRATOR__ODE_COLLECTION = eINSTANCE.getIntegrator_OdeCollection();

		/**
		 * The meta object literal for the '<em><b>Run</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INTEGRATOR___RUN = eINSTANCE.getIntegrator__Run();

		/**
		 * The meta object literal for the '{@link org.thirdeye.core.integrator.impl.EquationSystemImpl <em>Equation System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.integrator.impl.EquationSystemImpl
		 * @see org.thirdeye.core.integrator.impl.IntegratorPackageImpl#getEquationSystem()
		 * @generated
		 */
		EClass EQUATION_SYSTEM = eINSTANCE.getEquationSystem();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUATION_SYSTEM__Y = eINSTANCE.getEquationSystem_Y();

		/**
		 * The meta object literal for the '<em><b>YDot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUATION_SYSTEM__YDOT = eINSTANCE.getEquationSystem_YDot();

		/**
		 * The meta object literal for the '<em><b>Dimension</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUATION_SYSTEM__DIMENSION = eINSTANCE.getEquationSystem_Dimension();

	}

} //IntegratorPackage
