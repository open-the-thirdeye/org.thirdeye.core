/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.integrator;

import java.util.concurrent.CyclicBarrier;

import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;
import org.apache.commons.math3.ode.FirstOrderIntegrator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Apache Integrator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.thirdeye.core.integrator.ApacheIntegrator#getBarrier <em>Barrier</em>}</li>
 *   <li>{@link org.thirdeye.core.integrator.ApacheIntegrator#getIntegrator <em>Integrator</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.thirdeye.core.integrator.IntegratorPackage#getApacheIntegrator()
 * @model superTypes="org.thirdeye.core.dependencies.EFirstOrderDifferentialEquations org.thirdeye.core.integrator.Integrator"
 * @generated
 */
public interface ApacheIntegrator extends FirstOrderDifferentialEquations,
		Integrator {
	/**
	 * Returns the value of the '<em><b>Barrier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Barrier</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Barrier</em>' attribute.
	 * @see #setBarrier(CyclicBarrier)
	 * @see org.thirdeye.core.integrator.IntegratorPackage#getApacheIntegrator_Barrier()
	 * @model dataType="org.thirdeye.core.dependencies.ECyclicBarrier" required="true" transient="true"
	 * @generated
	 */
	CyclicBarrier getBarrier();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.integrator.ApacheIntegrator#getBarrier <em>Barrier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Barrier</em>' attribute.
	 * @see #getBarrier()
	 * @generated
	 */
	void setBarrier(CyclicBarrier value);

	/**
	 * Returns the value of the '<em><b>Integrator</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integrator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integrator</em>' attribute.
	 * @see org.thirdeye.core.integrator.IntegratorPackage#getApacheIntegrator_Integrator()
	 * @model default="" dataType="org.thirdeye.core.dependencies.EFirstOrderIntegrator" required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	FirstOrderIntegrator getIntegrator();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='getIntegrator().integrate(this, 0.0, getEquationSystem().getY(), 1.0, getEquationSystem().getY());\nSystem.out.println(\"Y:    \" + Arrays.toString(getEquationSystem().getY()));\nSystem.out.println(\"YDot: \" + Arrays.toString(getEquationSystem().getYDot()));'"
	 * @generated
	 */
	void run();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return getEquationSystem().getDimension();'"
	 * @generated
	 */
	int getDimension();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model tRequired="true" yDataType="org.thirdeye.core.dependencies.EDoubleArray" yRequired="true" yDotDataType="org.thirdeye.core.dependencies.EDoubleArray" yDotRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='for( int i=0;i<y.length;i++) {\n\tgetEquationSystem().getY()[i] = y[i];\t\t\t\n}\ntry {\n\tbarrier.await();\n} catch (InterruptedException | BrokenBarrierException e) {\n\t// TODO Auto-generated catch block\n\te.printStackTrace();\n}\nfor( int i=0;i<yDot.length;i++) {\n\tyDot[i] = getEquationSystem().getYDot()[i];\t\t\t\n}'"
	 * @generated
	 */
	void computeDerivatives(double t, double[] y, double[] yDot);

} // ApacheIntegrator
