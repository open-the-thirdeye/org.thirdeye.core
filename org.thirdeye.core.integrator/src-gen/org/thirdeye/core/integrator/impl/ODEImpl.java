/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.integrator.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.thirdeye.core.integrator.Integrator;
import org.thirdeye.core.integrator.IntegratorPackage;
import org.thirdeye.core.integrator.ODE;
import org.thirdeye.core.mesh.Accessor;
import org.thirdeye.core.mesh.Provider;
import org.thirdeye.core.mesh.impl.TransformationEdgeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ODE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.thirdeye.core.integrator.impl.ODEImpl#getIntegrator <em>Integrator</em>}</li>
 *   <li>{@link org.thirdeye.core.integrator.impl.ODEImpl#getEqIndex <em>Eq Index</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ODEImpl extends TransformationEdgeImpl implements ODE {
	/**
	 * The cached value of the '{@link #getIntegrator() <em>Integrator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegrator()
	 * @generated
	 * @ordered
	 */
	protected Integrator integrator;

	/**
	 * The default value of the '{@link #getEqIndex() <em>Eq Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEqIndex()
	 * @generated
	 * @ordered
	 */
	protected static final int EQ_INDEX_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEqIndex() <em>Eq Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEqIndex()
	 * @generated
	 * @ordered
	 */
	protected int eqIndex = EQ_INDEX_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ODEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IntegratorPackage.Literals.ODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integrator getIntegrator() {
		if (integrator != null && integrator.eIsProxy()) {
			InternalEObject oldIntegrator = (InternalEObject)integrator;
			integrator = (Integrator)eResolveProxy(oldIntegrator);
			if (integrator != oldIntegrator) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IntegratorPackage.ODE__INTEGRATOR, oldIntegrator, integrator));
			}
		}
		return integrator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integrator basicGetIntegrator() {
		return integrator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIntegrator(Integrator newIntegrator, NotificationChain msgs) {
		Integrator oldIntegrator = integrator;
		integrator = newIntegrator;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, IntegratorPackage.ODE__INTEGRATOR, oldIntegrator, newIntegrator);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegrator(Integrator newIntegrator) {
		if (newIntegrator != integrator) {
			NotificationChain msgs = null;
			if (integrator != null)
				msgs = ((InternalEObject)integrator).eInverseRemove(this, IntegratorPackage.INTEGRATOR__ODE_COLLECTION, Integrator.class, msgs);
			if (newIntegrator != null)
				msgs = ((InternalEObject)newIntegrator).eInverseAdd(this, IntegratorPackage.INTEGRATOR__ODE_COLLECTION, Integrator.class, msgs);
			msgs = basicSetIntegrator(newIntegrator, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IntegratorPackage.ODE__INTEGRATOR, newIntegrator, newIntegrator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getEqIndex() {
		return eqIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEqIndex(int newEqIndex) {
		int oldEqIndex = eqIndex;
		eqIndex = newEqIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IntegratorPackage.ODE__EQ_INDEX, oldEqIndex, eqIndex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("rawtypes")
	public void getDerivativeFromVar() {
		Object temp = getFrom().getVar().get(getFrom().getIndex());
		if( temp instanceof EList ) {
			int i = 0;
			for( Object obj: (EList)temp ) {
				getIntegrator().getEquationSystem().getYDot()[getEqIndex()+i] = (double)obj;
				i++;
			}
		} else {
			getIntegrator().getEquationSystem().getYDot()[getEqIndex()] = (double)temp;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	public void setIntegralToVar() {
		Object fromObj;
		if( getFrom().getIndex() == -1 ) {										//Var is not an EList element
			Object temp = getFrom().getVar().get(getFrom().getIndex());
			if(temp instanceof EList) {										//If Var is EList =>
				Double[] tempArray = new Double[((EList<Double>)temp).size() ];
				for( int i = 0; i < tempArray.length; i++ ) {					//Iterate over list
					tempArray[i] = getIntegrator().getEquationSystem().getY()[getEqIndex()+i];	
				}
				fromObj = tempArray;
			} else {														//Var is POD => means can be cast to double
				fromObj = getIntegrator().getEquationSystem().getY()[getEqIndex()];	
			}
		} else {															//Var is an Elist element => means can be cast to double
			fromObj = getIntegrator().getEquationSystem().getY()[getEqIndex()];	
		}
		
		Object tempObj = fromObj;
		for( Provider prov: getProviders() ) {
			tempObj = prov.apply(tempObj);
		}
		for( Accessor access: getTo() ) {
			access.getVar().set(access.getIndex(),tempObj);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case IntegratorPackage.ODE__INTEGRATOR:
				if (integrator != null)
					msgs = ((InternalEObject)integrator).eInverseRemove(this, IntegratorPackage.INTEGRATOR__ODE_COLLECTION, Integrator.class, msgs);
				return basicSetIntegrator((Integrator)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case IntegratorPackage.ODE__INTEGRATOR:
				return basicSetIntegrator(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case IntegratorPackage.ODE__INTEGRATOR:
				if (resolve) return getIntegrator();
				return basicGetIntegrator();
			case IntegratorPackage.ODE__EQ_INDEX:
				return getEqIndex();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case IntegratorPackage.ODE__INTEGRATOR:
				setIntegrator((Integrator)newValue);
				return;
			case IntegratorPackage.ODE__EQ_INDEX:
				setEqIndex((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case IntegratorPackage.ODE__INTEGRATOR:
				setIntegrator((Integrator)null);
				return;
			case IntegratorPackage.ODE__EQ_INDEX:
				setEqIndex(EQ_INDEX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case IntegratorPackage.ODE__INTEGRATOR:
				return integrator != null;
			case IntegratorPackage.ODE__EQ_INDEX:
				return eqIndex != EQ_INDEX_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
			case IntegratorPackage.ODE___GET_DERIVATIVE_FROM_VAR:
				getDerivativeFromVar();
				return null;
			case IntegratorPackage.ODE___SET_INTEGRAL_TO_VAR:
				setIntegralToVar();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (eqIndex: ");
		result.append(eqIndex);
		result.append(')');
		return result.toString();
	}

} //ODEImpl
