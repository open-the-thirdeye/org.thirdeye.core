/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.integrator.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.thirdeye.core.integrator.EquationSystem;
import org.thirdeye.core.integrator.Integrator;
import org.thirdeye.core.integrator.IntegratorPackage;
import org.thirdeye.core.integrator.ODE;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Integrator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.thirdeye.core.integrator.impl.IntegratorImpl#getEquationSystem <em>Equation System</em>}</li>
 *   <li>{@link org.thirdeye.core.integrator.impl.IntegratorImpl#getOdeCollection <em>Ode Collection</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class IntegratorImpl extends MinimalEObjectImpl.Container
		implements Integrator {
	/**
	 * The cached value of the '{@link #getOdeCollection() <em>Ode Collection</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOdeCollection()
	 * @generated
	 * @ordered
	 */
	protected EList<ODE> odeCollection;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntegratorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IntegratorPackage.Literals.INTEGRATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Moved in order to make getEquationSystem abstract
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquationSystem getEquationSystemGen() {
		// TODO: implement this method to return the 'Equation System' containment reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * Moved in order to make basicSetEquationSystem abstract
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquationSystemGen(EquationSystem newEquationSystem, NotificationChain msgs) {
		// TODO: implement this method to set the contained 'Equation System' containment reference
		// -> this method is automatically invoked to keep the containment relationship in synch
		// -> do not modify other features
		// -> return msgs, after adding any generated Notification to it (if it is null, a NotificationChain object must be created first)
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * Needed by framework
	 * <!-- end-user-doc -->
	 * @generated NOT (Patch)
	 */
	private NotificationChain basicSetEquationSystem(Object object, NotificationChain msgs) {
		// Intentionally left Blank.
		throw new UnsupportedOperationException();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ODE> getOdeCollection() {
		if (odeCollection == null) {
			odeCollection = new EObjectWithInverseResolvingEList<ODE>(ODE.class, this, IntegratorPackage.INTEGRATOR__ODE_COLLECTION, IntegratorPackage.ODE__INTEGRATOR);
		}
		return odeCollection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Moved in order to make run abstract
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void runGen() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case IntegratorPackage.INTEGRATOR__ODE_COLLECTION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOdeCollection()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case IntegratorPackage.INTEGRATOR__EQUATION_SYSTEM:
				return basicSetEquationSystem(null, msgs);
			case IntegratorPackage.INTEGRATOR__ODE_COLLECTION:
				return ((InternalEList<?>)getOdeCollection()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case IntegratorPackage.INTEGRATOR__EQUATION_SYSTEM:
				return getEquationSystem();
			case IntegratorPackage.INTEGRATOR__ODE_COLLECTION:
				return getOdeCollection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case IntegratorPackage.INTEGRATOR__ODE_COLLECTION:
				getOdeCollection().clear();
				getOdeCollection().addAll((Collection<? extends ODE>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case IntegratorPackage.INTEGRATOR__ODE_COLLECTION:
				getOdeCollection().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case IntegratorPackage.INTEGRATOR__EQUATION_SYSTEM:
				return getEquationSystem() != null;
			case IntegratorPackage.INTEGRATOR__ODE_COLLECTION:
				return odeCollection != null && !odeCollection.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
			case IntegratorPackage.INTEGRATOR___RUN:
				run();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //IntegratorImpl
