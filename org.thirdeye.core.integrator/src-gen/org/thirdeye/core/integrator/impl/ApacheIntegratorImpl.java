/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.integrator.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.concurrent.CyclicBarrier;

import org.apache.commons.math3.ode.FirstOrderIntegrator;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.thirdeye.core.integrator.ApacheIntegrator;
import org.thirdeye.core.integrator.EquationSystem;
import org.thirdeye.core.integrator.Integrator;
import org.thirdeye.core.integrator.IntegratorPackage;
import org.thirdeye.core.integrator.ODE;



/**
 * <!-- begin-user-doc -->
 * Needed imports.
 * <!-- end-user-doc -->
 * @generated NOT (Patch)
 */
import java.util.concurrent.BrokenBarrierException;

import org.apache.commons.math3.ode.nonstiff.ClassicalRungeKuttaIntegrator;
import org.thirdeye.core.integrator.IntegratorFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Apache Integrator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.thirdeye.core.integrator.impl.ApacheIntegratorImpl#getEquationSystem <em>Equation System</em>}</li>
 *   <li>{@link org.thirdeye.core.integrator.impl.ApacheIntegratorImpl#getOdeCollection <em>Ode Collection</em>}</li>
 *   <li>{@link org.thirdeye.core.integrator.impl.ApacheIntegratorImpl#getBarrier <em>Barrier</em>}</li>
 *   <li>{@link org.thirdeye.core.integrator.impl.ApacheIntegratorImpl#getIntegrator <em>Integrator</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ApacheIntegratorImpl extends MinimalEObjectImpl.Container
		implements ApacheIntegrator {
	/**
	 * The cached value of the '{@link #getOdeCollection() <em>Ode Collection</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOdeCollection()
	 * @generated
	 * @ordered
	 */
	protected EList<ODE> odeCollection;

	/**
	 * The default value of the '{@link #getBarrier() <em>Barrier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBarrier()
	 * @generated
	 * @ordered
	 */
	protected static final CyclicBarrier BARRIER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBarrier() <em>Barrier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBarrier()
	 * @generated
	 * @ordered
	 */
	protected CyclicBarrier barrier = BARRIER_EDEFAULT;

	/**
	 * The default value of the '{@link #getIntegrator() <em>Integrator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * Set to null to avoid "cannot create from string exception"
	 * <!-- end-user-doc -->
	 * @see #getIntegrator()
	 * @generated NOT (Patch)
	 * @ordered
	 */
	protected static final FirstOrderIntegrator INTEGRATOR_EDEFAULT = null;
	
	/**
	 * The cached value of the '{@link #getIntegrator() <em>Integrator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * Needed because transient
	 * <!-- end-user-doc -->
	 * @see #getIntegrator()
	 * @generated NOT (Patch)
	 * @ordered
	 */
	protected FirstOrderIntegrator integrator = INTEGRATOR_EDEFAULT;
	
	/**
	 * The cached value of the '{@link #getEquationSystem() <em>EquationSystem</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * Needed because transient
	 * <!-- end-user-doc -->
	 * @see #getEquationSystem()
	 * @generated NOT (Patch)
	 * @ordered
	 */
	protected EquationSystem equationSystem = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApacheIntegratorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IntegratorPackage.Literals.APACHE_INTEGRATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Gets and generates the transient EquationSystem.
	 * <!-- end-user-doc -->
	 * @generated NOT (Patch)
	 */
	public EquationSystem getEquationSystem() {
		IntegratorPackageImpl.init();
		IntegratorFactory factory = IntegratorFactory.eINSTANCE;

		if( equationSystem == null ) {
			equationSystem = factory.createEquationSystem();
			equationSystem.setDimension(getOdeCollection().size());
		}
		return equationSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT (Patch)
	 */
	public NotificationChain basicSetEquationSystem(EquationSystem newEquationSystem, NotificationChain msgs) {
		EquationSystem oldValue = equationSystem;
		equationSystem = newEquationSystem;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, IntegratorPackage.EQUATION_SYSTEM, oldValue, newEquationSystem);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ODE> getOdeCollection() {
		if (odeCollection == null) {
			odeCollection = new EObjectWithInverseResolvingEList<ODE>(ODE.class, this, IntegratorPackage.APACHE_INTEGRATOR__ODE_COLLECTION, IntegratorPackage.ODE__INTEGRATOR);
		}
		return odeCollection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CyclicBarrier getBarrier() {
		return barrier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBarrier(CyclicBarrier newBarrier) {
		CyclicBarrier oldBarrier = barrier;
		barrier = newBarrier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IntegratorPackage.APACHE_INTEGRATOR__BARRIER, oldBarrier, barrier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * Gets and generates the transient Integrator.
	 * <!-- end-user-doc -->
	 * @generated NOT (Patch)
	 */
	public FirstOrderIntegrator getIntegrator() {
		if( integrator == null ) {
			integrator = new ClassicalRungeKuttaIntegrator(1.0);
		}
		return integrator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void run() {
		getIntegrator().integrate(this, 0.0, getEquationSystem().getY(), 1.0, getEquationSystem().getY());
		//System.out.println("Y:    " + Arrays.toString(getEquationSystem().getY()));
		//System.out.println("YDot: " + Arrays.toString(getEquationSystem().getYDot()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDimension() {
		return getEquationSystem().getDimension();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void computeDerivatives(final double t, final double[] y, final double[] yDot) {
		for( int i=0;i<y.length;i++) {
			getEquationSystem().getY()[i] = y[i];			
		}
		try {
			barrier.await();
		} catch (InterruptedException | BrokenBarrierException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for( int i=0;i<yDot.length;i++) {
			yDot[i] = getEquationSystem().getYDot()[i];			
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case IntegratorPackage.APACHE_INTEGRATOR__ODE_COLLECTION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOdeCollection()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case IntegratorPackage.APACHE_INTEGRATOR__EQUATION_SYSTEM:
				return basicSetEquationSystem(null, msgs);
			case IntegratorPackage.APACHE_INTEGRATOR__ODE_COLLECTION:
				return ((InternalEList<?>)getOdeCollection()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case IntegratorPackage.APACHE_INTEGRATOR__EQUATION_SYSTEM:
				return getEquationSystem();
			case IntegratorPackage.APACHE_INTEGRATOR__ODE_COLLECTION:
				return getOdeCollection();
			case IntegratorPackage.APACHE_INTEGRATOR__BARRIER:
				return getBarrier();
			case IntegratorPackage.APACHE_INTEGRATOR__INTEGRATOR:
				return getIntegrator();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case IntegratorPackage.APACHE_INTEGRATOR__ODE_COLLECTION:
				getOdeCollection().clear();
				getOdeCollection().addAll((Collection<? extends ODE>)newValue);
				return;
			case IntegratorPackage.APACHE_INTEGRATOR__BARRIER:
				setBarrier((CyclicBarrier)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case IntegratorPackage.APACHE_INTEGRATOR__ODE_COLLECTION:
				getOdeCollection().clear();
				return;
			case IntegratorPackage.APACHE_INTEGRATOR__BARRIER:
				setBarrier(BARRIER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case IntegratorPackage.APACHE_INTEGRATOR__EQUATION_SYSTEM:
				return getEquationSystem() != null;
			case IntegratorPackage.APACHE_INTEGRATOR__ODE_COLLECTION:
				return odeCollection != null && !odeCollection.isEmpty();
			case IntegratorPackage.APACHE_INTEGRATOR__BARRIER:
				return BARRIER_EDEFAULT == null ? barrier != null : !BARRIER_EDEFAULT.equals(barrier);
			case IntegratorPackage.APACHE_INTEGRATOR__INTEGRATOR:
				return INTEGRATOR_EDEFAULT == null ? getIntegrator() != null : !INTEGRATOR_EDEFAULT.equals(getIntegrator());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Runnable.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Integrator.class) {
			switch (derivedFeatureID) {
				case IntegratorPackage.APACHE_INTEGRATOR__EQUATION_SYSTEM: return IntegratorPackage.INTEGRATOR__EQUATION_SYSTEM;
				case IntegratorPackage.APACHE_INTEGRATOR__ODE_COLLECTION: return IntegratorPackage.INTEGRATOR__ODE_COLLECTION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Runnable.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Integrator.class) {
			switch (baseFeatureID) {
				case IntegratorPackage.INTEGRATOR__EQUATION_SYSTEM: return IntegratorPackage.APACHE_INTEGRATOR__EQUATION_SYSTEM;
				case IntegratorPackage.INTEGRATOR__ODE_COLLECTION: return IntegratorPackage.APACHE_INTEGRATOR__ODE_COLLECTION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == Runnable.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		if (baseClass == Integrator.class) {
			switch (baseOperationID) {
				case IntegratorPackage.INTEGRATOR___RUN: return IntegratorPackage.APACHE_INTEGRATOR___RUN;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
			case IntegratorPackage.APACHE_INTEGRATOR___RUN:
				run();
				return null;
			case IntegratorPackage.APACHE_INTEGRATOR___GET_DIMENSION:
				return getDimension();
			case IntegratorPackage.APACHE_INTEGRATOR___COMPUTE_DERIVATIVES__DOUBLE_DOUBLE_DOUBLE:
				computeDerivatives((Double)arguments.get(0), (double[])arguments.get(1), (double[])arguments.get(2));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (barrier: ");
		result.append(barrier);
		result.append(')');
		return result.toString();
	}

} //ApacheIntegratorImpl
