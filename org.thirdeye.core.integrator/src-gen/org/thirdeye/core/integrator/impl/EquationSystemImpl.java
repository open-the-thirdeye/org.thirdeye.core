/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.integrator.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.thirdeye.core.integrator.EquationSystem;
import org.thirdeye.core.integrator.IntegratorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equation System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.thirdeye.core.integrator.impl.EquationSystemImpl#getY <em>Y</em>}</li>
 *   <li>{@link org.thirdeye.core.integrator.impl.EquationSystemImpl#getYDot <em>YDot</em>}</li>
 *   <li>{@link org.thirdeye.core.integrator.impl.EquationSystemImpl#getDimension <em>Dimension</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EquationSystemImpl extends MinimalEObjectImpl.Container implements
		EquationSystem {
	/**
	 * The default value of the '{@link #getY() <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * Set to null to avoid "cannot create from string exception"
	 * <!-- end-user-doc -->
	 * @see #getY()
	 * @generated NOT (Patch)
	 * @ordered
	 */
	protected static final double[] Y_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getY() <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * Needed because transient
	 * <!-- end-user-doc -->
	 * @see #getY()
	 * @generated NOT (Patch)
	 * @ordered
	 */
	protected double[] y = Y_EDEFAULT;

	/**
	 * The default value of the '{@link #getYDot() <em>YDot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * Set to null to avoid "cannot create from string exception"
	 * <!-- end-user-doc -->
	 * @see #getYDot()
	 * @generated NOT (Patch)
	 * @ordered
	 */
	protected static final double[] YDOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getYDot() <em>YDot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * Needed because transient
	 * <!-- end-user-doc -->
	 * @see #getYDot()
	 * @generated NOT (Patch)
	 * @ordered
	 */
	protected double[] yDot = YDOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getDimension() <em>Dimension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDimension()
	 * @generated
	 * @ordered
	 */
	protected static final int DIMENSION_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDimension() <em>Dimension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDimension()
	 * @generated
	 * @ordered
	 */
	protected int dimension = DIMENSION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EquationSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IntegratorPackage.Literals.EQUATION_SYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Gets and generates the transient y.
	 * <!-- end-user-doc -->
	 * @generated NOT (Patch)
	 */
	public double[] getY() {
		if( y  == null ) {
			y = new double[getDimension()];
		}
		return y;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Gets and generates the transient yDot.
	 * <!-- end-user-doc -->
	 * @generated NOT (Patch)
	 */
	public double[] getYDot() {
		if( yDot  == null ) {
			yDot = new double[getDimension()];
		}
		return yDot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDimension() {
		return dimension;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDimension(int newDimension) {
		int oldDimension = dimension;
		dimension = newDimension;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IntegratorPackage.EQUATION_SYSTEM__DIMENSION, oldDimension, dimension));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case IntegratorPackage.EQUATION_SYSTEM__Y:
				return getY();
			case IntegratorPackage.EQUATION_SYSTEM__YDOT:
				return getYDot();
			case IntegratorPackage.EQUATION_SYSTEM__DIMENSION:
				return getDimension();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case IntegratorPackage.EQUATION_SYSTEM__DIMENSION:
				setDimension((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case IntegratorPackage.EQUATION_SYSTEM__DIMENSION:
				setDimension(DIMENSION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case IntegratorPackage.EQUATION_SYSTEM__Y:
				return Y_EDEFAULT == null ? getY() != null : !Y_EDEFAULT.equals(getY());
			case IntegratorPackage.EQUATION_SYSTEM__YDOT:
				return YDOT_EDEFAULT == null ? getYDot() != null : !YDOT_EDEFAULT.equals(getYDot());
			case IntegratorPackage.EQUATION_SYSTEM__DIMENSION:
				return dimension != DIMENSION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (dimension: ");
		result.append(dimension);
		result.append(')');
		return result.toString();
	}

} //EquationSystemImpl
