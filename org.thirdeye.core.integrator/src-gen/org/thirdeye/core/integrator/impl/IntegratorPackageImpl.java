/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.integrator.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.thirdeye.core.dependencies.DependenciesPackage;
import org.thirdeye.core.integrator.ApacheIntegrator;
import org.thirdeye.core.integrator.EquationSystem;
import org.thirdeye.core.integrator.Integrator;
import org.thirdeye.core.integrator.IntegratorFactory;
import org.thirdeye.core.integrator.IntegratorPackage;
import org.thirdeye.core.mesh.MeshPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class IntegratorPackageImpl extends EPackageImpl implements
		IntegratorPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass odeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass apacheIntegratorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass integratorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equationSystemEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.thirdeye.core.integrator.IntegratorPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private IntegratorPackageImpl() {
		super(eNS_URI, IntegratorFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link IntegratorPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static IntegratorPackage init() {
		if (isInited) return (IntegratorPackage)EPackage.Registry.INSTANCE.getEPackage(IntegratorPackage.eNS_URI);

		// Obtain or create and register package
		IntegratorPackageImpl theIntegratorPackage = (IntegratorPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof IntegratorPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new IntegratorPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		MeshPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theIntegratorPackage.createPackageContents();

		// Initialize created meta-data
		theIntegratorPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theIntegratorPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(IntegratorPackage.eNS_URI, theIntegratorPackage);
		return theIntegratorPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getODE() {
		return odeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getODE_Integrator() {
		return (EReference)odeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getODE_EqIndex() {
		return (EAttribute)odeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getODE__GetDerivativeFromVar() {
		return odeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getODE__SetIntegralToVar() {
		return odeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApacheIntegrator() {
		return apacheIntegratorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getApacheIntegrator_Barrier() {
		return (EAttribute)apacheIntegratorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getApacheIntegrator_Integrator() {
		return (EAttribute)apacheIntegratorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApacheIntegrator__Run() {
		return apacheIntegratorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApacheIntegrator__GetDimension() {
		return apacheIntegratorEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApacheIntegrator__ComputeDerivatives__double_double_double() {
		return apacheIntegratorEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntegrator() {
		return integratorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIntegrator_EquationSystem() {
		return (EReference)integratorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIntegrator_OdeCollection() {
		return (EReference)integratorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIntegrator__Run() {
		return integratorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquationSystem() {
		return equationSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquationSystem_Y() {
		return (EAttribute)equationSystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquationSystem_YDot() {
		return (EAttribute)equationSystemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquationSystem_Dimension() {
		return (EAttribute)equationSystemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegratorFactory getIntegratorFactory() {
		return (IntegratorFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		odeEClass = createEClass(ODE);
		createEReference(odeEClass, ODE__INTEGRATOR);
		createEAttribute(odeEClass, ODE__EQ_INDEX);
		createEOperation(odeEClass, ODE___GET_DERIVATIVE_FROM_VAR);
		createEOperation(odeEClass, ODE___SET_INTEGRAL_TO_VAR);

		apacheIntegratorEClass = createEClass(APACHE_INTEGRATOR);
		createEAttribute(apacheIntegratorEClass, APACHE_INTEGRATOR__BARRIER);
		createEAttribute(apacheIntegratorEClass, APACHE_INTEGRATOR__INTEGRATOR);
		createEOperation(apacheIntegratorEClass, APACHE_INTEGRATOR___RUN);
		createEOperation(apacheIntegratorEClass, APACHE_INTEGRATOR___GET_DIMENSION);
		createEOperation(apacheIntegratorEClass, APACHE_INTEGRATOR___COMPUTE_DERIVATIVES__DOUBLE_DOUBLE_DOUBLE);

		integratorEClass = createEClass(INTEGRATOR);
		createEReference(integratorEClass, INTEGRATOR__EQUATION_SYSTEM);
		createEReference(integratorEClass, INTEGRATOR__ODE_COLLECTION);
		createEOperation(integratorEClass, INTEGRATOR___RUN);

		equationSystemEClass = createEClass(EQUATION_SYSTEM);
		createEAttribute(equationSystemEClass, EQUATION_SYSTEM__Y);
		createEAttribute(equationSystemEClass, EQUATION_SYSTEM__YDOT);
		createEAttribute(equationSystemEClass, EQUATION_SYSTEM__DIMENSION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MeshPackage theMeshPackage = (MeshPackage)EPackage.Registry.INSTANCE.getEPackage(MeshPackage.eNS_URI);
		DependenciesPackage theDependenciesPackage = (DependenciesPackage)EPackage.Registry.INSTANCE.getEPackage(DependenciesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		odeEClass.getESuperTypes().add(theMeshPackage.getTransformationEdge());
		apacheIntegratorEClass.getESuperTypes().add(theDependenciesPackage.getEFirstOrderDifferentialEquations());
		apacheIntegratorEClass.getESuperTypes().add(this.getIntegrator());
		integratorEClass.getESuperTypes().add(theDependenciesPackage.getERunnable());

		// Initialize classes, features, and operations; add parameters
		initEClass(odeEClass, org.thirdeye.core.integrator.ODE.class, "ODE", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getODE_Integrator(), this.getIntegrator(), this.getIntegrator_OdeCollection(), "integrator", null, 1, 1, org.thirdeye.core.integrator.ODE.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getODE_EqIndex(), ecorePackage.getEInt(), "eqIndex", null, 1, 1, org.thirdeye.core.integrator.ODE.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getODE__GetDerivativeFromVar(), null, "getDerivativeFromVar", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getODE__SetIntegralToVar(), null, "setIntegralToVar", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(apacheIntegratorEClass, ApacheIntegrator.class, "ApacheIntegrator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getApacheIntegrator_Barrier(), theDependenciesPackage.getECyclicBarrier(), "barrier", null, 1, 1, ApacheIntegrator.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getApacheIntegrator_Integrator(), theDependenciesPackage.getEFirstOrderIntegrator(), "integrator", "", 1, 1, ApacheIntegrator.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEOperation(getApacheIntegrator__Run(), null, "run", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getApacheIntegrator__GetDimension(), ecorePackage.getEInt(), "getDimension", 1, 1, IS_UNIQUE, IS_ORDERED);

		EOperation op = initEOperation(getApacheIntegrator__ComputeDerivatives__double_double_double(), null, "computeDerivatives", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "t", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theDependenciesPackage.getEDoubleArray(), "y", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theDependenciesPackage.getEDoubleArray(), "yDot", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(integratorEClass, Integrator.class, "Integrator", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIntegrator_EquationSystem(), this.getEquationSystem(), null, "equationSystem", null, 1, 1, Integrator.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getIntegrator_OdeCollection(), this.getODE(), this.getODE_Integrator(), "odeCollection", null, 0, -1, Integrator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getIntegrator__Run(), null, "run", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(equationSystemEClass, EquationSystem.class, "EquationSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEquationSystem_Y(), theDependenciesPackage.getEDoubleArray(), "y", "", 1, 1, EquationSystem.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquationSystem_YDot(), theDependenciesPackage.getEDoubleArray(), "yDot", "", 1, 1, EquationSystem.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquationSystem_Dimension(), ecorePackage.getEInt(), "dimension", null, 1, 1, EquationSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //IntegratorPackageImpl
