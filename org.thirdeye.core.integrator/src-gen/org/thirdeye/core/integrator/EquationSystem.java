/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.integrator;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equation System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.thirdeye.core.integrator.EquationSystem#getY <em>Y</em>}</li>
 *   <li>{@link org.thirdeye.core.integrator.EquationSystem#getYDot <em>YDot</em>}</li>
 *   <li>{@link org.thirdeye.core.integrator.EquationSystem#getDimension <em>Dimension</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.thirdeye.core.integrator.IntegratorPackage#getEquationSystem()
 * @model
 * @generated
 */
public interface EquationSystem extends EObject {
	/**
	 * Returns the value of the '<em><b>Y</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Y</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y</em>' attribute.
	 * @see org.thirdeye.core.integrator.IntegratorPackage#getEquationSystem_Y()
	 * @model default="" dataType="org.thirdeye.core.dependencies.EDoubleArray" required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	double[] getY();

	/**
	 * Returns the value of the '<em><b>YDot</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>YDot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>YDot</em>' attribute.
	 * @see org.thirdeye.core.integrator.IntegratorPackage#getEquationSystem_YDot()
	 * @model default="" dataType="org.thirdeye.core.dependencies.EDoubleArray" required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	double[] getYDot();

	/**
	 * Returns the value of the '<em><b>Dimension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dimension</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dimension</em>' attribute.
	 * @see #setDimension(int)
	 * @see org.thirdeye.core.integrator.IntegratorPackage#getEquationSystem_Dimension()
	 * @model required="true"
	 * @generated
	 */
	int getDimension();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.integrator.EquationSystem#getDimension <em>Dimension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dimension</em>' attribute.
	 * @see #getDimension()
	 * @generated
	 */
	void setDimension(int value);

} // EquationSystem
