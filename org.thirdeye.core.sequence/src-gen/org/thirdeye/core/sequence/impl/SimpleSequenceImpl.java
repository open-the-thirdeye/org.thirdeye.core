/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.sequence.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.concurrent.CyclicBarrier;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.thirdeye.core.mesh.Edge;
import org.thirdeye.core.mesh.Node;
import org.thirdeye.core.integrator.ApacheIntegrator;
import org.thirdeye.core.integrator.Integrator;
import org.thirdeye.core.integrator.IntegratorFactory;
import org.thirdeye.core.integrator.ODE;
import org.thirdeye.core.integrator.impl.IntegratorPackageImpl;
import org.thirdeye.core.sequence.SequencePackage;
import org.thirdeye.core.sequence.SimpleSequence;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple Sequence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.thirdeye.core.sequence.impl.SimpleSequenceImpl#getBarrier <em>Barrier</em>}</li>
 *   <li>{@link org.thirdeye.core.sequence.impl.SimpleSequenceImpl#getIntegrator <em>Integrator</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SimpleSequenceImpl extends SequenceImpl implements SimpleSequence {
	/**
	 * The default value of the '{@link #getBarrier() <em>Barrier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBarrier()
	 * @generated
	 * @ordered
	 */
	protected static final CyclicBarrier BARRIER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBarrier() <em>Barrier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBarrier()
	 * @generated
	 * @ordered
	 */
	protected CyclicBarrier barrier = BARRIER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIntegrator() <em>Integrator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegrator()
	 * @generated
	 * @ordered
	 */
	protected Integrator integrator;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimpleSequenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SequencePackage.Literals.SIMPLE_SEQUENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CyclicBarrier getBarrier() {
		return barrier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBarrier(CyclicBarrier newBarrier) {
		CyclicBarrier oldBarrier = barrier;
		barrier = newBarrier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SequencePackage.SIMPLE_SEQUENCE__BARRIER, oldBarrier, barrier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integrator getIntegrator() {
		return integrator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIntegrator(Integrator newIntegrator, NotificationChain msgs) {
		Integrator oldIntegrator = integrator;
		integrator = newIntegrator;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SequencePackage.SIMPLE_SEQUENCE__INTEGRATOR, oldIntegrator, newIntegrator);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegrator(Integrator newIntegrator) {
		if (newIntegrator != integrator) {
			NotificationChain msgs = null;
			if (integrator != null)
				msgs = ((InternalEObject)integrator).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SequencePackage.SIMPLE_SEQUENCE__INTEGRATOR, null, msgs);
			if (newIntegrator != null)
				msgs = ((InternalEObject)newIntegrator).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SequencePackage.SIMPLE_SEQUENCE__INTEGRATOR, null, msgs);
			msgs = basicSetIntegrator(newIntegrator, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SequencePackage.SIMPLE_SEQUENCE__INTEGRATOR, newIntegrator, newIntegrator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override public void step() {
		getIntegrator().run();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void run() {
		//Get Integrals from Integrator and Set to Variables
		for (Edge edge : getMesh().getIntegratorEdges()) {
			((ODE)edge).setIntegralToVar();
		}
		
		//Distribute values to Provider and do Provider calc
		for (Edge edge : getMesh().getToProviderEdges()) {
			edge.calc();
		}
		for (Node node : getMesh().getProviderNodes()) {
			node.calc();
		}
		
		//Distribute values from Models to Connections and Models and do Connection calc
		for (Edge edge : getMesh().getFromModelEdges()) {
			edge.calc();
		}
		for (Node node : getMesh().getConnectionNodes()) {
			node.calc();
		}
		
		//Distribute values from Connections to Models
		for (Edge edge : getMesh().getFromConnectionEdges()) {
			edge.calc();
		}
		//All done, do Model Calc
		for (Node node : getMesh().getModelNodes()) {
			node.calc();
		}
		//Get Derivatives from Variables and Set to Integrator
		for (Edge edge : getMesh().getIntegratorEdges()) {
			((ODE)edge).getDerivativeFromVar();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	public void init() {
		// TODO: Remove this function by making barrier transient
		EList<Double> tempList = new BasicEList<Double>();
		EList<Integer> intList = new BasicEList<Integer>();
		IntegratorPackageImpl.init();
		IntegratorFactory factory = IntegratorFactory.eINSTANCE;
		ApacheIntegrator integ = factory.createApacheIntegrator();
		/*
		System.out.println("Models");
		for( Node node: getMesh().getModelNodes()) {
			for( Variable var: node.getVariables() ) {
				for( Accessor access: var.getAccessors() ) {
					System.out.println(access.toString());
				}
			}
		}
		System.out.println("Connections");
		for( Node node: getMesh().getConnectionNodes()) { 
			for( Variable var: node.getVariables() ) {
				for( Accessor access: var.getAccessors() ) {
					System.out.println(access.toString());
				}
			}
		}
		System.out.println("Providers");
		for( Node node: getMesh().getProviderNodes()) { 
			for( Variable var: node.getVariables() ) {
				for( Accessor access: var.getAccessors() ) {
					System.out.println(access.toString());
				}
			}
		}
		*/
		
		for( Edge ode: getMesh().getIntegratorEdges()) {
			((ODE) ode).setIntegrator(integ);
			/*
			System.out.println(ode.toString());
			System.out.println(ode.getTo().toString());
			System.out.println(ode.getTo().get(0).toString());
			System.out.println(ode.getTo().get(0).getIndex());
			System.out.println(ode.getTo().get(0).getVar().toString());
			*/
			Object obj= ode.getTo().get(0).getVar().get(ode.getTo().get(0).getIndex());
			if( obj instanceof EList) {
				EList<Double> temp = (EList<Double>)obj;
				for( int i = 0; i < temp.size(); i++ ) {					//Iterate over list
					//System.out.println("Setting Y[" + (((ODE) ode).getEqIndex()+i) + "] to " + (double)temp.get(i) );
					intList.add(((ODE) ode).getEqIndex()+i);
					tempList.add((double)temp.get(i));
				}
			} else {
				if( obj != null ) {
					//System.out.println("Setting Y[" + (((ODE) ode).getEqIndex()) + "] to " + (double)obj );
					intList.add(((ODE) ode).getEqIndex());
					tempList.add((double)obj);
				} else {
					//System.out.println("obj = null! ODE = " + ((ODE) ode).toString());
				}
			}
		}
		integ.getEquationSystem().setDimension(tempList.size());
		int i = 0;
		for( double d: tempList ) {
			integ.getEquationSystem().getY()[intList.get(i)] = d;
			i++;
		}
		for( Edge ode: getMesh().getIntegratorEdges()) {
			((ODE) ode).getDerivativeFromVar();
		}
		setIntegrator(integ);
		setBarrier(new CyclicBarrier( 1, this ) );
		
		((ApacheIntegrator)getIntegrator()).setBarrier(getBarrier());
		for (Node node : getMesh().getProviderNodes()) {
			node.calc();
		}
		System.out.println("Y:    " + Arrays.toString(integ.getEquationSystem().getY()));
		System.out.println("YDot: " + Arrays.toString(integ.getEquationSystem().getYDot()));

	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SequencePackage.SIMPLE_SEQUENCE__INTEGRATOR:
				return basicSetIntegrator(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SequencePackage.SIMPLE_SEQUENCE__BARRIER:
				return getBarrier();
			case SequencePackage.SIMPLE_SEQUENCE__INTEGRATOR:
				return getIntegrator();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SequencePackage.SIMPLE_SEQUENCE__BARRIER:
				setBarrier((CyclicBarrier)newValue);
				return;
			case SequencePackage.SIMPLE_SEQUENCE__INTEGRATOR:
				setIntegrator((Integrator)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SequencePackage.SIMPLE_SEQUENCE__BARRIER:
				setBarrier(BARRIER_EDEFAULT);
				return;
			case SequencePackage.SIMPLE_SEQUENCE__INTEGRATOR:
				setIntegrator((Integrator)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SequencePackage.SIMPLE_SEQUENCE__BARRIER:
				return BARRIER_EDEFAULT == null ? barrier != null : !BARRIER_EDEFAULT.equals(barrier);
			case SequencePackage.SIMPLE_SEQUENCE__INTEGRATOR:
				return integrator != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case SequencePackage.SIMPLE_SEQUENCE___RUN:
				run();
				return null;
			case SequencePackage.SIMPLE_SEQUENCE___INIT:
				init();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (barrier: ");
		result.append(barrier);
		result.append(')');
		return result.toString();
	}

} //SimpleSequenceImpl
