/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.sequence;

import java.util.concurrent.CyclicBarrier;
import org.thirdeye.core.integrator.Integrator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.thirdeye.core.sequence.SimpleSequence#getBarrier <em>Barrier</em>}</li>
 *   <li>{@link org.thirdeye.core.sequence.SimpleSequence#getIntegrator <em>Integrator</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.thirdeye.core.sequence.SequencePackage#getSimpleSequence()
 * @model superTypes="org.thirdeye.core.sequence.Sequence org.thirdeye.core.dependencies.ERunnable"
 * @generated
 */
public interface SimpleSequence extends Sequence, Runnable {
	/**
	 * Returns the value of the '<em><b>Barrier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Barrier</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Barrier</em>' attribute.
	 * @see #setBarrier(CyclicBarrier)
	 * @see org.thirdeye.core.sequence.SequencePackage#getSimpleSequence_Barrier()
	 * @model dataType="org.thirdeye.core.dependencies.ECyclicBarrier" required="true" transient="true"
	 * @generated
	 */
	CyclicBarrier getBarrier();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.sequence.SimpleSequence#getBarrier <em>Barrier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Barrier</em>' attribute.
	 * @see #getBarrier()
	 * @generated
	 */
	void setBarrier(CyclicBarrier value);

	/**
	 * Returns the value of the '<em><b>Integrator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integrator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integrator</em>' containment reference.
	 * @see #setIntegrator(Integrator)
	 * @see org.thirdeye.core.sequence.SequencePackage#getSimpleSequence_Integrator()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Integrator getIntegrator();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.sequence.SimpleSequence#getIntegrator <em>Integrator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integrator</em>' containment reference.
	 * @see #getIntegrator()
	 * @generated
	 */
	void setIntegrator(Integrator value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void run();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init();

} // SimpleSequence
