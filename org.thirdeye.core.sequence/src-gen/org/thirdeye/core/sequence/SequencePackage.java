/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.sequence;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.thirdeye.core.sequence.SequenceFactory
 * @model kind="package"
 * @generated
 */
public interface SequencePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "sequence";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://sequence/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "sequence";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SequencePackage eINSTANCE = org.thirdeye.core.sequence.impl.SequencePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.thirdeye.core.sequence.impl.SequenceImpl <em>Sequence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.sequence.impl.SequenceImpl
	 * @see org.thirdeye.core.sequence.impl.SequencePackageImpl#getSequence()
	 * @generated
	 */
	int SEQUENCE = 0;

	/**
	 * The feature id for the '<em><b>Mesh</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__MESH = 0;

	/**
	 * The number of structural features of the '<em>Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE___STEP = 0;

	/**
	 * The number of operations of the '<em>Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.core.sequence.impl.SimpleSequenceImpl <em>Simple Sequence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.sequence.impl.SimpleSequenceImpl
	 * @see org.thirdeye.core.sequence.impl.SequencePackageImpl#getSimpleSequence()
	 * @generated
	 */
	int SIMPLE_SEQUENCE = 1;

	/**
	 * The feature id for the '<em><b>Mesh</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SEQUENCE__MESH = SEQUENCE__MESH;

	/**
	 * The feature id for the '<em><b>Barrier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SEQUENCE__BARRIER = SEQUENCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Integrator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SEQUENCE__INTEGRATOR = SEQUENCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Simple Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SEQUENCE_FEATURE_COUNT = SEQUENCE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SEQUENCE___STEP = SEQUENCE___STEP;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SEQUENCE___RUN = SEQUENCE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SEQUENCE___INIT = SEQUENCE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Simple Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SEQUENCE_OPERATION_COUNT = SEQUENCE_OPERATION_COUNT + 2;


	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.sequence.Sequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequence</em>'.
	 * @see org.thirdeye.core.sequence.Sequence
	 * @generated
	 */
	EClass getSequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.thirdeye.core.sequence.Sequence#getMesh <em>Mesh</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Mesh</em>'.
	 * @see org.thirdeye.core.sequence.Sequence#getMesh()
	 * @see #getSequence()
	 * @generated
	 */
	EReference getSequence_Mesh();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.sequence.Sequence#step() <em>Step</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Step</em>' operation.
	 * @see org.thirdeye.core.sequence.Sequence#step()
	 * @generated
	 */
	EOperation getSequence__Step();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.sequence.SimpleSequence <em>Simple Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Sequence</em>'.
	 * @see org.thirdeye.core.sequence.SimpleSequence
	 * @generated
	 */
	EClass getSimpleSequence();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.sequence.SimpleSequence#getBarrier <em>Barrier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Barrier</em>'.
	 * @see org.thirdeye.core.sequence.SimpleSequence#getBarrier()
	 * @see #getSimpleSequence()
	 * @generated
	 */
	EAttribute getSimpleSequence_Barrier();

	/**
	 * Returns the meta object for the containment reference '{@link org.thirdeye.core.sequence.SimpleSequence#getIntegrator <em>Integrator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Integrator</em>'.
	 * @see org.thirdeye.core.sequence.SimpleSequence#getIntegrator()
	 * @see #getSimpleSequence()
	 * @generated
	 */
	EReference getSimpleSequence_Integrator();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.sequence.SimpleSequence#run() <em>Run</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Run</em>' operation.
	 * @see org.thirdeye.core.sequence.SimpleSequence#run()
	 * @generated
	 */
	EOperation getSimpleSequence__Run();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.sequence.SimpleSequence#init() <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.thirdeye.core.sequence.SimpleSequence#init()
	 * @generated
	 */
	EOperation getSimpleSequence__Init();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SequenceFactory getSequenceFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.thirdeye.core.sequence.impl.SequenceImpl <em>Sequence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.sequence.impl.SequenceImpl
		 * @see org.thirdeye.core.sequence.impl.SequencePackageImpl#getSequence()
		 * @generated
		 */
		EClass SEQUENCE = eINSTANCE.getSequence();

		/**
		 * The meta object literal for the '<em><b>Mesh</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE__MESH = eINSTANCE.getSequence_Mesh();

		/**
		 * The meta object literal for the '<em><b>Step</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SEQUENCE___STEP = eINSTANCE.getSequence__Step();

		/**
		 * The meta object literal for the '{@link org.thirdeye.core.sequence.impl.SimpleSequenceImpl <em>Simple Sequence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.sequence.impl.SimpleSequenceImpl
		 * @see org.thirdeye.core.sequence.impl.SequencePackageImpl#getSimpleSequence()
		 * @generated
		 */
		EClass SIMPLE_SEQUENCE = eINSTANCE.getSimpleSequence();

		/**
		 * The meta object literal for the '<em><b>Barrier</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_SEQUENCE__BARRIER = eINSTANCE.getSimpleSequence_Barrier();

		/**
		 * The meta object literal for the '<em><b>Integrator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMPLE_SEQUENCE__INTEGRATOR = eINSTANCE.getSimpleSequence_Integrator();

		/**
		 * The meta object literal for the '<em><b>Run</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SIMPLE_SEQUENCE___RUN = eINSTANCE.getSimpleSequence__Run();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SIMPLE_SEQUENCE___INIT = eINSTANCE.getSimpleSequence__Init();

	}

} //SequencePackage
