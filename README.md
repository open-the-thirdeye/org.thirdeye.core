This is the README of the ThirdEye!

How to configure your Eclipse to develop the ThirdEye:

* Create a new folder "org.thirdeye" cd into "org.thirdeye".

    * git clone org.thirdeye.core
    * git clone org.thirdeye.ui
    * git clone org.thirdeye.extension
    * git clone org.thirdeye.extension.ui

* start eclipse and point the workspace to "org.thirdeye"

    * create project New->Project->General/Project name it: "org.thirdeye.core"
    * create project New->Project->General/Project name it: "org.thirdeye.ui"
    * create project New->Project->General/Project name it: "org.thirdeye.extension"
    * create project New->Project->General/Project name it: "org.thirdeye.extension.ui"

* now you should see 4 projects with sub-projects in them.

* right click on a sub-project and select "import as project" go through all sub-projects.

* select "Projects Presentation: Hierarchical" in "Project Explorer" settings dialog
