/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.persistence;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.Resource;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Persistence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.core.persistence.Persistence#getBackend <em>Backend</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.core.persistence.PersistencePackage#getPersistence()
 * @model
 * @generated
 */
public interface Persistence extends EObject {
	/**
	 * Returns the value of the '<em><b>Backend</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Backend</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Backend</em>' containment reference.
	 * @see #setBackend(Backend)
	 * @see org.thirdeye.core.persistence.PersistencePackage#getPersistence_Backend()
	 * @model containment="true" required="true" transient="true" volatile="true" derived="true"
	 * @generated
	 */
	Backend getBackend();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.persistence.Persistence#getBackend <em>Backend</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Backend</em>' containment reference.
	 * @see #getBackend()
	 * @generated
	 */
	void setBackend(Backend value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model objectsDataType="org.thirdeye.core.persistence.EJavaObjectArray" objectsRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='for( Object obj: objects) {\n\tgetResource().getContents().add((EObject)obj);\n}'"
	 * @generated
	 */
	void save(Object[] objects);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.thirdeye.core.persistence.EJavaObjectArray" required="true"
	 * @generated
	 */
	Object[] load();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return getBackend().getLocalResource();'"
	 * @generated
	 */
	Resource getResource();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='try {\n\tgetResource().save(Collections.EMPTY_MAP);\n} catch (IOException e) {\n\te.printStackTrace();\n}'"
	 * @generated
	 */
	void doSave();

} // Persistence
