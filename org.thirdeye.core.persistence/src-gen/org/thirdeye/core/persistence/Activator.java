/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.persistence;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.thirdeye.core.persistence.impl.PersistencePackageImpl;

public class Activator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("org.thirdeye.core.persistence:Activator");
		PersistencePackageImpl.init();
		PersistenceFactory factory = PersistenceFactory.eINSTANCE;
		Persistence persistence = factory.createPersistence();
		XMLBackend backend = factory.createXMLBackend();
		persistence.setBackend(backend);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		// TODO Auto-generated method stub

	}

}
