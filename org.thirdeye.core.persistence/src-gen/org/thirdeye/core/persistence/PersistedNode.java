/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.persistence;

import org.eclipse.emf.ecore.EObject;

import org.thirdeye.core.mesh.Node;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Persisted Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.core.persistence.PersistedNode#getNode <em>Node</em>}</li>
 *   <li>{@link org.thirdeye.core.persistence.PersistedNode#getSimulationStep <em>Simulation Step</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.core.persistence.PersistencePackage#getPersistedNode()
 * @model
 * @generated
 */
public interface PersistedNode extends EObject {
	/**
	 * Returns the value of the '<em><b>Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node</em>' containment reference.
	 * @see #setNode(Node)
	 * @see org.thirdeye.core.persistence.PersistencePackage#getPersistedNode_Node()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Node getNode();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.persistence.PersistedNode#getNode <em>Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node</em>' containment reference.
	 * @see #getNode()
	 * @generated
	 */
	void setNode(Node value);

	/**
	 * Returns the value of the '<em><b>Simulation Step</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simulation Step</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simulation Step</em>' attribute.
	 * @see #setSimulationStep(long)
	 * @see org.thirdeye.core.persistence.PersistencePackage#getPersistedNode_SimulationStep()
	 * @model default="0" required="true"
	 * @generated
	 */
	long getSimulationStep();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.persistence.PersistedNode#getSimulationStep <em>Simulation Step</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simulation Step</em>' attribute.
	 * @see #getSimulationStep()
	 * @generated
	 */
	void setSimulationStep(long value);

} // PersistedNode
