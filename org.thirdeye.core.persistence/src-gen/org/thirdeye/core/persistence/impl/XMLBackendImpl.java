/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.persistence.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.UUID;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
//import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.thirdeye.core.mesh.Mesh;
import org.thirdeye.core.mesh.Variable;
import org.thirdeye.core.persistence.PersistedNode;
import org.thirdeye.core.persistence.PersistencePackage;
import org.thirdeye.core.persistence.XMLBackend;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XML Backend</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class XMLBackendImpl extends BackendImpl implements XMLBackend {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XMLBackendImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PersistencePackage.Literals.XML_BACKEND;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource createResource() {
		//System.out.println("createResource");

		String uuid = UUID.randomUUID().toString();
		String fileName = "test_" + uuid;
		String fileExtension = "run";

		String projectName = "persistence";
		
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put(fileExtension, new XMIResourceFactoryImpl());
		
		ResourceSet resSet = new ResourceSetImpl();

		//TransactionalEditingDomain domain = TransactionalEditingDomain.Factory.INSTANCE.createEditingDomain(resSet);

		IWorkspaceRoot myWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		
		IProject myProject = myWorkspaceRoot.getProject(projectName);
		if(myProject.exists() && !myProject.isOpen()) {
			try {
				myProject.open(null);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		IFile myFile = myProject.getFile(fileName + "." + fileExtension);
		return resSet.createResource(URI.createPlatformResourceURI(myFile.getFullPath().toString(), true));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case PersistencePackage.XML_BACKEND___CREATE_RESOURCE:
				return createResource();
		}
		return super.eInvoke(operationID, arguments);
	}

	@Override
	public EList<Mesh> getStoredMeshes() {
		//System.out.println("XMLBackend::getStoredMeshes");	

		String fileExtension = "mesh";
		String projectName = "persistence";
		
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put(fileExtension, new XMIResourceFactoryImpl());
		ResourceSet resSet = new ResourceSetImpl();
		
		//@SuppressWarnings("unused")
		//TransactionalEditingDomain domain = TransactionalEditingDomain.Factory.INSTANCE.createEditingDomain(resSet);
		
		IWorkspaceRoot myWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		
		EList<Mesh> list = new BasicEList<Mesh>();

		IProject[] myProjects = myWorkspaceRoot.getProjects();
		for( IProject proj: myProjects ) {
			if(proj.isOpen()) {
				try {
					if( proj.getFolder(projectName).exists() ) {
						for (IResource resource : proj.getFolder(projectName).members()) {
							if(resource.getFullPath().toString().endsWith(fileExtension)) {
								Resource emfResource = resSet.getResource(URI.createURI(resource.getLocationURI().toString()), true);
								//System.out.println(emfResource.toString());
								list.add((Mesh)emfResource.getContents().get(0));
							}
						}
					}
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
/*		IProject myProject = myWorkspaceRoot.getProject(projectName);

		if(myProject.exists() && !myProject.isOpen()) {
			try {
				myProject.open(null);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(myProject.isOpen()) {
			try {
				for (IResource resource : myProject.members()) {
					if(resource.getFullPath().toString().endsWith(fileExtension)) {
						Resource emfResource = resSet.getResource(URI.createURI(resource.getLocationURI().toString()), true);
						//System.out.println(emfResource.toString());
						list.add((Mesh)emfResource.getContents().get(0));
					}
				}
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/

		return list;
	}

	@Override
	public EList<EObject> getStoredRunners() {
		//System.out.println("XMLBackend::getStoredRunners");	

		String fileExtension = "persistence";
		String projectName = "persistence";
		
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put(fileExtension, new XMIResourceFactoryImpl());
		ResourceSet resSet = new ResourceSetImpl();

		//@SuppressWarnings("unused")
		//TransactionalEditingDomain domain = TransactionalEditingDomain.Factory.INSTANCE.createEditingDomain(resSet);

		IWorkspaceRoot myWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		
		EList<EObject> list = new BasicEList<EObject>();

		IProject[] myProjects = myWorkspaceRoot.getProjects();
		for( IProject proj: myProjects ) {
			if(proj.isOpen()) {
				try {
					if( proj.getFolder(projectName).exists() ) {
						for (IResource resource : proj.getFolder(projectName).members()) {
							if(resource.getFullPath().toString().endsWith(fileExtension)) {
								Resource emfResource = resSet.getResource(URI.createURI(resource.getLocationURI().toString()), true);
								//System.out.println(emfResource.toString());
								list.addAll(emfResource.getContents());
							}
						}
					}
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
/*		IProject myProject = myWorkspaceRoot.getProject(projectName);

		if(myProject.exists() && !myProject.isOpen()) {
			try {
				myProject.open(null);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		EList<EObject> list = new BasicEList<EObject>();
		if(myProject.isOpen()) {
			try {
				for (IResource resource : myProject.members()) {
					if(resource.getFullPath().toString().endsWith(fileExtension)) {
						Resource emfResource = resSet.getResource(URI.createURI(resource.getLocationURI().toString()), true);
						//System.out.println(emfResource.toString());
						list.addAll(emfResource.getContents());
					}
				}
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/
		return list;
	}

	@Override
	public void storeMesh(Mesh mesh) {
		System.out.println("XMLBackend::storeMesh");	

	}

	@Override
	public EList<EObject> getStoredNodes(Variable var, String id ) {
		//System.out.println("XMLBackend::getStoredNodes");	
		//System.out.println("runnerID = " + runnerID);	
		//System.out.println("var Name = " + var.getFeature());
		
		String fileName =  id.substring(id.lastIndexOf('/')+1,id.lastIndexOf('.')) + "_Test";
		System.out.println("fileName = " + fileName );
		String fileExtension = "run";
		
		String[] tempString = id.split("/");
		String projectName = tempString[tempString.length-3];
		System.out.println("projectName = " + projectName );
		
		//String fileExtension = "run";
		//String projectName = "persistence";
		
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put(fileExtension, new XMIResourceFactoryImpl());
		ResourceSet resSet = new ResourceSetImpl();

		//@SuppressWarnings("unused")
		//TransactionalEditingDomain domain = TransactionalEditingDomain.Factory.INSTANCE.createEditingDomain(resSet);

		IWorkspaceRoot myWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		IProject myProject = myWorkspaceRoot.getProject(projectName);

		if(myProject.exists() && !myProject.isOpen()) {
			try {
				myProject.open(null);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		EList<EObject> list = new BasicEList<EObject>();
		EList<EObject> resultList = new BasicEList<EObject>();
		if(myProject.isOpen()) {
			try {
				for (IResource resource : myProject.getFolder("persistence").members()) {
					System.out.println(resource.getName());

					if(resource.getFullPath().toString().endsWith(fileName + "." + fileExtension)) {
						Resource emfResource = resSet.getResource(URI.createURI(resource.getLocationURI().toString()), true);
						System.out.println(emfResource.toString());
						list.addAll(emfResource.getContents());
					}
				}
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		for( EObject temp: list) {
			PersistedNode node = (PersistedNode) temp;
			if( node.getNode().getName().equals(var.getNode().getName()) ) {
				//System.out.println(node.getNode().getName() + " step: " + node.getSimulationStep());
				resultList.add(node);
			}
				
		}
		return resultList;
	}

	@Override
	public void storeRunner(EObject runner) {
		System.out.println("storeRunner");	
	}

	@Override
	public Resource createResource(String id) {
		//System.out.println("createResource(String id)");

		String fileName =  id.substring(id.lastIndexOf('/')+1,id.length());
		//System.out.println("fileName = " + fileName );
		String fileExtension = "run";
		
		String[] temp = id.split("/");
		String projectName = temp[temp.length-3];
		//System.out.println("projectName = " + projectName );
		
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put(fileExtension, new XMIResourceFactoryImpl());
		
		ResourceSet resSet = new ResourceSetImpl();
		
		//TransactionalEditingDomain domain = TransactionalEditingDomain.Factory.INSTANCE.createEditingDomain(resSet);

		IWorkspaceRoot myWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		
		IProject myProject = myWorkspaceRoot.getProject(projectName);
		if(myProject.exists() && !myProject.isOpen()) {
			try {
				myProject.open(null);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		IFile myFile = myProject.getFile("persistence/" + fileName + "." + fileExtension);
		System.out.println("Persistence resource = " + myFile.getFullPath().toString() );
		return resSet.createResource(URI.createPlatformResourceURI(myFile.getFullPath().toString(), true));
	}

} //XMLBackendImpl
