/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.persistence.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.thirdeye.core.dependencies.DependenciesPackage;
import org.thirdeye.core.mesh.MeshPackage;

import org.thirdeye.core.persistence.Backend;
import org.thirdeye.core.persistence.PersistedNode;
import org.thirdeye.core.persistence.Persistence;
import org.thirdeye.core.persistence.PersistenceFactory;
import org.thirdeye.core.persistence.PersistencePackage;
import org.thirdeye.core.persistence.SimulationRun;
import org.thirdeye.core.persistence.XMLBackend;
import org.thirdeye.core.sequence.SequencePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PersistencePackageImpl extends EPackageImpl implements PersistencePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass persistedNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass persistenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass backendEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlBackendEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simulationRunEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eJavaObjectArrayEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.thirdeye.core.persistence.PersistencePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private PersistencePackageImpl() {
		super(eNS_URI, PersistenceFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link PersistencePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static PersistencePackage init() {
		if (isInited) return (PersistencePackage)EPackage.Registry.INSTANCE.getEPackage(PersistencePackage.eNS_URI);

		// Obtain or create and register package
		PersistencePackageImpl thePersistencePackage = (PersistencePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof PersistencePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new PersistencePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		SequencePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		thePersistencePackage.createPackageContents();

		// Initialize created meta-data
		thePersistencePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		thePersistencePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(PersistencePackage.eNS_URI, thePersistencePackage);
		return thePersistencePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersistedNode() {
		return persistedNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersistedNode_Node() {
		return (EReference)persistedNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersistedNode_SimulationStep() {
		return (EAttribute)persistedNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersistence() {
		return persistenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersistence_Backend() {
		return (EReference)persistenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPersistence__Save__Object() {
		return persistenceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPersistence__Load() {
		return persistenceEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPersistence__GetResource() {
		return persistenceEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPersistence__DoSave() {
		return persistenceEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBackend() {
		return backendEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBackend_LocalResource() {
		return (EAttribute)backendEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBackend__CreateResource() {
		return backendEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBackend__GetStoredNodes__Variable_String() {
		return backendEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBackend__GetStoredMeshes() {
		return backendEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBackend__GetStoredRunners() {
		return backendEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBackend__StoreMesh__Mesh() {
		return backendEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBackend__StoreRunner__EObject() {
		return backendEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLBackend() {
		return xmlBackendEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXMLBackend__CreateResource() {
		return xmlBackendEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimulationRun() {
		return simulationRunEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulationRun_Name() {
		return (EAttribute)simulationRunEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulationRun_Description() {
		return (EAttribute)simulationRunEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulationRun_GitVersion() {
		return (EAttribute)simulationRunEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulationRun_StartTime() {
		return (EAttribute)simulationRunEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulationRun_Steps() {
		return (EAttribute)simulationRunEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulationRun_Running() {
		return (EAttribute)simulationRunEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulationRun_Sequence() {
		return (EReference)simulationRunEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSimulationRun__Run() {
		return simulationRunEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEJavaObjectArray() {
		return eJavaObjectArrayEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersistenceFactory getPersistenceFactory() {
		return (PersistenceFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		persistedNodeEClass = createEClass(PERSISTED_NODE);
		createEReference(persistedNodeEClass, PERSISTED_NODE__NODE);
		createEAttribute(persistedNodeEClass, PERSISTED_NODE__SIMULATION_STEP);

		persistenceEClass = createEClass(PERSISTENCE);
		createEReference(persistenceEClass, PERSISTENCE__BACKEND);
		createEOperation(persistenceEClass, PERSISTENCE___SAVE__OBJECT);
		createEOperation(persistenceEClass, PERSISTENCE___LOAD);
		createEOperation(persistenceEClass, PERSISTENCE___GET_RESOURCE);
		createEOperation(persistenceEClass, PERSISTENCE___DO_SAVE);

		backendEClass = createEClass(BACKEND);
		createEAttribute(backendEClass, BACKEND__LOCAL_RESOURCE);
		createEOperation(backendEClass, BACKEND___CREATE_RESOURCE);
		createEOperation(backendEClass, BACKEND___GET_STORED_NODES__VARIABLE_STRING);
		createEOperation(backendEClass, BACKEND___GET_STORED_MESHES);
		createEOperation(backendEClass, BACKEND___GET_STORED_RUNNERS);
		createEOperation(backendEClass, BACKEND___STORE_MESH__MESH);
		createEOperation(backendEClass, BACKEND___STORE_RUNNER__EOBJECT);

		xmlBackendEClass = createEClass(XML_BACKEND);
		createEOperation(xmlBackendEClass, XML_BACKEND___CREATE_RESOURCE);

		simulationRunEClass = createEClass(SIMULATION_RUN);
		createEAttribute(simulationRunEClass, SIMULATION_RUN__NAME);
		createEAttribute(simulationRunEClass, SIMULATION_RUN__DESCRIPTION);
		createEAttribute(simulationRunEClass, SIMULATION_RUN__GIT_VERSION);
		createEAttribute(simulationRunEClass, SIMULATION_RUN__START_TIME);
		createEAttribute(simulationRunEClass, SIMULATION_RUN__STEPS);
		createEAttribute(simulationRunEClass, SIMULATION_RUN__RUNNING);
		createEReference(simulationRunEClass, SIMULATION_RUN__SEQUENCE);
		createEOperation(simulationRunEClass, SIMULATION_RUN___RUN);

		// Create data types
		eJavaObjectArrayEDataType = createEDataType(EJAVA_OBJECT_ARRAY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MeshPackage theMeshPackage = (MeshPackage)EPackage.Registry.INSTANCE.getEPackage(MeshPackage.eNS_URI);
		DependenciesPackage theDependenciesPackage = (DependenciesPackage)EPackage.Registry.INSTANCE.getEPackage(DependenciesPackage.eNS_URI);
		SequencePackage theSequencePackage = (SequencePackage)EPackage.Registry.INSTANCE.getEPackage(SequencePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		xmlBackendEClass.getESuperTypes().add(this.getBackend());
		simulationRunEClass.getESuperTypes().add(theDependenciesPackage.getERunnable());

		// Initialize classes, features, and operations; add parameters
		initEClass(persistedNodeEClass, PersistedNode.class, "PersistedNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersistedNode_Node(), theMeshPackage.getNode(), null, "node", null, 1, 1, PersistedNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersistedNode_SimulationStep(), ecorePackage.getELong(), "simulationStep", "0", 1, 1, PersistedNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(persistenceEClass, Persistence.class, "Persistence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersistence_Backend(), this.getBackend(), null, "backend", null, 1, 1, Persistence.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getPersistence__Save__Object(), null, "save", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEJavaObjectArray(), "objects", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getPersistence__Load(), this.getEJavaObjectArray(), "load", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getPersistence__GetResource(), ecorePackage.getEResource(), "getResource", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getPersistence__DoSave(), null, "doSave", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(backendEClass, Backend.class, "Backend", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBackend_LocalResource(), ecorePackage.getEResource(), "localResource", null, 1, 1, Backend.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEOperation(getBackend__CreateResource(), ecorePackage.getEResource(), "createResource", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getBackend__GetStoredNodes__Variable_String(), null, "getStoredNodes", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMeshPackage.getVariable(), "var", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "runnerID", 1, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEEList());
		EGenericType g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getBackend__GetStoredMeshes(), null, "getStoredMeshes", 1, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEEList());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getBackend__GetStoredRunners(), null, "getStoredRunners", 1, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEEList());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getBackend__StoreMesh__Mesh(), null, "storeMesh", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMeshPackage.getMesh(), "mesh", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getBackend__StoreRunner__EObject(), null, "storeRunner", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "runner", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(xmlBackendEClass, XMLBackend.class, "XMLBackend", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getXMLBackend__CreateResource(), ecorePackage.getEResource(), "createResource", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(simulationRunEClass, SimulationRun.class, "SimulationRun", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSimulationRun_Name(), ecorePackage.getEString(), "name", null, 1, 1, SimulationRun.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulationRun_Description(), ecorePackage.getEString(), "description", null, 1, 1, SimulationRun.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulationRun_GitVersion(), ecorePackage.getEString(), "gitVersion", null, 1, 1, SimulationRun.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulationRun_StartTime(), ecorePackage.getEDate(), "startTime", null, 1, 1, SimulationRun.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulationRun_Steps(), ecorePackage.getELong(), "steps", null, 1, 1, SimulationRun.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulationRun_Running(), ecorePackage.getEBoolean(), "running", null, 1, 1, SimulationRun.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulationRun_Sequence(), theSequencePackage.getSequence(), null, "sequence", null, 1, 1, SimulationRun.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getSimulationRun__Run(), null, "run", 1, 1, IS_UNIQUE, IS_ORDERED);

		// Initialize data types
		initEDataType(eJavaObjectArrayEDataType, Object[].class, "EJavaObjectArray", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //PersistencePackageImpl
