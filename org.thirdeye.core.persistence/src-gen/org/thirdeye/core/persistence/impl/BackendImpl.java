/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.persistence.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.resource.Resource;

import org.thirdeye.core.mesh.Mesh;
import org.thirdeye.core.mesh.Variable;
import org.thirdeye.core.persistence.Backend;
import org.thirdeye.core.persistence.PersistencePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Backend</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.core.persistence.impl.BackendImpl#getLocalResource <em>Local Resource</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class BackendImpl extends MinimalEObjectImpl.Container implements Backend {
	/**
	 * The default value of the '{@link #getLocalResource() <em>Local Resource</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalResource()
	 * @generated
	 * @ordered
	 */
	protected static final Resource LOCAL_RESOURCE_EDEFAULT = null;
	protected Resource localResource;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BackendImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PersistencePackage.Literals.BACKEND;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Resource createLocalResource(String id) {
		//System.out.println(this.toString() + " createLocalResource(String id): Setting resource to " + id );
		localResource = createResource(id);
		return localResource;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Resource getLocalResource() {
		//System.out.println(this.toString() + " getLocalResource()" );
		if( localResource ==null ) {
			localResource = createResource();
		}
		return localResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource createResourceGen() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<?> getStoredNodes(Variable var, String runnerID) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<?> getStoredMeshesGen() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<?> getStoredRunnersGen() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void storeMeshGen(Mesh mesh) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void storeRunner(EObject runner) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PersistencePackage.BACKEND__LOCAL_RESOURCE:
				return getLocalResource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PersistencePackage.BACKEND__LOCAL_RESOURCE:
				return LOCAL_RESOURCE_EDEFAULT == null ? getLocalResource() != null : !LOCAL_RESOURCE_EDEFAULT.equals(getLocalResource());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case PersistencePackage.BACKEND___CREATE_RESOURCE:
				return createResource();
			case PersistencePackage.BACKEND___GET_STORED_NODES__VARIABLE_STRING:
				return getStoredNodes((Variable)arguments.get(0), (String)arguments.get(1));
			case PersistencePackage.BACKEND___GET_STORED_MESHES:
				return getStoredMeshes();
			case PersistencePackage.BACKEND___GET_STORED_RUNNERS:
				return getStoredRunners();
			case PersistencePackage.BACKEND___STORE_MESH__MESH:
				storeMesh((Mesh)arguments.get(0));
				return null;
			case PersistencePackage.BACKEND___STORE_RUNNER__EOBJECT:
				storeRunner((EObject)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //BackendImpl
