/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.persistence.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.FeatureMap.Entry;
import org.thirdeye.core.mesh.Node;
import org.thirdeye.core.persistence.PersistedNode;
import org.thirdeye.core.persistence.Persistence;
import org.thirdeye.core.persistence.PersistenceFactory;
import org.thirdeye.core.persistence.PersistencePackage;
import org.thirdeye.core.persistence.SimulationRun;

import org.thirdeye.core.sequence.Sequence;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simulation Run</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.core.persistence.impl.SimulationRunImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.thirdeye.core.persistence.impl.SimulationRunImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.thirdeye.core.persistence.impl.SimulationRunImpl#getGitVersion <em>Git Version</em>}</li>
 *   <li>{@link org.thirdeye.core.persistence.impl.SimulationRunImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link org.thirdeye.core.persistence.impl.SimulationRunImpl#getSteps <em>Steps</em>}</li>
 *   <li>{@link org.thirdeye.core.persistence.impl.SimulationRunImpl#isRunning <em>Running</em>}</li>
 *   <li>{@link org.thirdeye.core.persistence.impl.SimulationRunImpl#getSequence <em>Sequence</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SimulationRunImpl extends MinimalEObjectImpl.Container implements SimulationRun {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getGitVersion() <em>Git Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGitVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String GIT_VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGitVersion() <em>Git Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGitVersion()
	 * @generated
	 * @ordered
	 */
	protected String gitVersion = GIT_VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected static final Date START_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected Date startTime = START_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSteps() <em>Steps</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSteps()
	 * @generated
	 * @ordered
	 */
	protected static final long STEPS_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getSteps() <em>Steps</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSteps()
	 * @generated
	 * @ordered
	 */
	protected long steps = STEPS_EDEFAULT;

	/**
	 * The default value of the '{@link #isRunning() <em>Running</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRunning()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RUNNING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRunning() <em>Running</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRunning()
	 * @generated
	 * @ordered
	 */
	protected boolean running = RUNNING_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSequence() <em>Sequence</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSequence()
	 * @generated
	 * @ordered
	 */
	protected Sequence sequence;

	private long currentStep;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimulationRunImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PersistencePackage.Literals.SIMULATION_RUN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PersistencePackage.SIMULATION_RUN__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PersistencePackage.SIMULATION_RUN__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getGitVersion() {
		return gitVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGitVersion(String newGitVersion) {
		String oldGitVersion = gitVersion;
		gitVersion = newGitVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PersistencePackage.SIMULATION_RUN__GIT_VERSION, oldGitVersion, gitVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(Date newStartTime) {
		Date oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PersistencePackage.SIMULATION_RUN__START_TIME, oldStartTime, startTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getSteps() {
		return steps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSteps(long newSteps) {
		long oldSteps = steps;
		steps = newSteps;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PersistencePackage.SIMULATION_RUN__STEPS, oldSteps, steps));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRunning(boolean newRunning) {
		boolean oldRunning = running;
		running = newRunning;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PersistencePackage.SIMULATION_RUN__RUNNING, oldRunning, running));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Sequence getSequence() {
		return sequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSequence(Sequence newSequence, NotificationChain msgs) {
		Sequence oldSequence = sequence;
		sequence = newSequence;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PersistencePackage.SIMULATION_RUN__SEQUENCE, oldSequence, newSequence);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSequence(Sequence newSequence) {
		if (newSequence != sequence) {
			NotificationChain msgs = null;
			if (sequence != null)
				msgs = ((InternalEObject)sequence).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PersistencePackage.SIMULATION_RUN__SEQUENCE, null, msgs);
			if (newSequence != null)
				msgs = ((InternalEObject)newSequence).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PersistencePackage.SIMULATION_RUN__SEQUENCE, null, msgs);
			msgs = basicSetSequence(newSequence, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PersistencePackage.SIMULATION_RUN__SEQUENCE, newSequence, newSequence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void run() {
		PersistencePackageImpl.init();
		PersistenceFactory factory = PersistenceFactory.eINSTANCE;
		Persistence persistence = factory.createPersistence();
		
		long nextStop = getSteps() + currentStep;
		//System.out.println("currentStep = " + currentStep + " next stop = " + nextStop );
		do {
			
			if( persistence.getBackend() != null) {
				EList<PersistedNode> models = new BasicEList<PersistedNode>();
				for( Entry entry: getSequence().getMesh().getNodes() ) {
					Node node = (Node) entry.getValue();
					PersistedNode persisted = PersistenceFactory.eINSTANCE.createPersistedNode();
					persisted.setNode((Node) EcoreUtil.copy(node));
					//System.out.println("currentStep = " + currentStep);
					persisted.setSimulationStep(currentStep+1);
					models.add(persisted);
				}
				persistence.save(models.toArray());
			}
/*			TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getSequence().getMesh());
			if (domain != null) {
				domain.getCommandStack().execute(new RecordingCommand(domain) {
					   public void doExecute() {
						   getSequence().step();
					   }
					});
			} else {*/
				getSequence().step();
//			}
			currentStep++;
		} while( isRunning() && (currentStep < nextStop ));
/*		if( core.getPersistence() != null) {
			core.getPersistence().doSave();
		}*/
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PersistencePackage.SIMULATION_RUN__SEQUENCE:
				return basicSetSequence(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PersistencePackage.SIMULATION_RUN__NAME:
				return getName();
			case PersistencePackage.SIMULATION_RUN__DESCRIPTION:
				return getDescription();
			case PersistencePackage.SIMULATION_RUN__GIT_VERSION:
				return getGitVersion();
			case PersistencePackage.SIMULATION_RUN__START_TIME:
				return getStartTime();
			case PersistencePackage.SIMULATION_RUN__STEPS:
				return getSteps();
			case PersistencePackage.SIMULATION_RUN__RUNNING:
				return isRunning();
			case PersistencePackage.SIMULATION_RUN__SEQUENCE:
				return getSequence();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PersistencePackage.SIMULATION_RUN__NAME:
				setName((String)newValue);
				return;
			case PersistencePackage.SIMULATION_RUN__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case PersistencePackage.SIMULATION_RUN__GIT_VERSION:
				setGitVersion((String)newValue);
				return;
			case PersistencePackage.SIMULATION_RUN__START_TIME:
				setStartTime((Date)newValue);
				return;
			case PersistencePackage.SIMULATION_RUN__STEPS:
				setSteps((Long)newValue);
				return;
			case PersistencePackage.SIMULATION_RUN__RUNNING:
				setRunning((Boolean)newValue);
				return;
			case PersistencePackage.SIMULATION_RUN__SEQUENCE:
				setSequence((Sequence)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PersistencePackage.SIMULATION_RUN__NAME:
				setName(NAME_EDEFAULT);
				return;
			case PersistencePackage.SIMULATION_RUN__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case PersistencePackage.SIMULATION_RUN__GIT_VERSION:
				setGitVersion(GIT_VERSION_EDEFAULT);
				return;
			case PersistencePackage.SIMULATION_RUN__START_TIME:
				setStartTime(START_TIME_EDEFAULT);
				return;
			case PersistencePackage.SIMULATION_RUN__STEPS:
				setSteps(STEPS_EDEFAULT);
				return;
			case PersistencePackage.SIMULATION_RUN__RUNNING:
				setRunning(RUNNING_EDEFAULT);
				return;
			case PersistencePackage.SIMULATION_RUN__SEQUENCE:
				setSequence((Sequence)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PersistencePackage.SIMULATION_RUN__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case PersistencePackage.SIMULATION_RUN__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case PersistencePackage.SIMULATION_RUN__GIT_VERSION:
				return GIT_VERSION_EDEFAULT == null ? gitVersion != null : !GIT_VERSION_EDEFAULT.equals(gitVersion);
			case PersistencePackage.SIMULATION_RUN__START_TIME:
				return START_TIME_EDEFAULT == null ? startTime != null : !START_TIME_EDEFAULT.equals(startTime);
			case PersistencePackage.SIMULATION_RUN__STEPS:
				return steps != STEPS_EDEFAULT;
			case PersistencePackage.SIMULATION_RUN__RUNNING:
				return running != RUNNING_EDEFAULT;
			case PersistencePackage.SIMULATION_RUN__SEQUENCE:
				return sequence != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case PersistencePackage.SIMULATION_RUN___RUN:
				run();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", description: ");
		result.append(description);
		result.append(", gitVersion: ");
		result.append(gitVersion);
		result.append(", startTime: ");
		result.append(startTime);
		result.append(", steps: ");
		result.append(steps);
		result.append(", running: ");
		result.append(running);
		result.append(')');
		return result.toString();
	}

} //SimulationRunImpl
