/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.persistence;

import java.util.Date;

import org.eclipse.emf.ecore.EObject;

import org.thirdeye.core.sequence.Sequence;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simulation Run</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.core.persistence.SimulationRun#getName <em>Name</em>}</li>
 *   <li>{@link org.thirdeye.core.persistence.SimulationRun#getDescription <em>Description</em>}</li>
 *   <li>{@link org.thirdeye.core.persistence.SimulationRun#getGitVersion <em>Git Version</em>}</li>
 *   <li>{@link org.thirdeye.core.persistence.SimulationRun#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link org.thirdeye.core.persistence.SimulationRun#getSteps <em>Steps</em>}</li>
 *   <li>{@link org.thirdeye.core.persistence.SimulationRun#isRunning <em>Running</em>}</li>
 *   <li>{@link org.thirdeye.core.persistence.SimulationRun#getSequence <em>Sequence</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.core.persistence.PersistencePackage#getSimulationRun()
 * @model superTypes="org.thirdeye.core.dependencies.ERunnable"
 * @generated
 */
public interface SimulationRun extends EObject, Runnable {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.thirdeye.core.persistence.PersistencePackage#getSimulationRun_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.persistence.SimulationRun#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see org.thirdeye.core.persistence.PersistencePackage#getSimulationRun_Description()
	 * @model required="true"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.persistence.SimulationRun#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Git Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Git Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Git Version</em>' attribute.
	 * @see #setGitVersion(String)
	 * @see org.thirdeye.core.persistence.PersistencePackage#getSimulationRun_GitVersion()
	 * @model required="true"
	 * @generated
	 */
	String getGitVersion();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.persistence.SimulationRun#getGitVersion <em>Git Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Git Version</em>' attribute.
	 * @see #getGitVersion()
	 * @generated
	 */
	void setGitVersion(String value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(Date)
	 * @see org.thirdeye.core.persistence.PersistencePackage#getSimulationRun_StartTime()
	 * @model required="true"
	 * @generated
	 */
	Date getStartTime();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.persistence.SimulationRun#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(Date value);

	/**
	 * Returns the value of the '<em><b>Steps</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Steps</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Steps</em>' attribute.
	 * @see #setSteps(long)
	 * @see org.thirdeye.core.persistence.PersistencePackage#getSimulationRun_Steps()
	 * @model required="true"
	 * @generated
	 */
	long getSteps();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.persistence.SimulationRun#getSteps <em>Steps</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Steps</em>' attribute.
	 * @see #getSteps()
	 * @generated
	 */
	void setSteps(long value);

	/**
	 * Returns the value of the '<em><b>Running</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Running</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Running</em>' attribute.
	 * @see #setRunning(boolean)
	 * @see org.thirdeye.core.persistence.PersistencePackage#getSimulationRun_Running()
	 * @model required="true"
	 * @generated
	 */
	boolean isRunning();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.persistence.SimulationRun#isRunning <em>Running</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Running</em>' attribute.
	 * @see #isRunning()
	 * @generated
	 */
	void setRunning(boolean value);

	/**
	 * Returns the value of the '<em><b>Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sequence</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sequence</em>' containment reference.
	 * @see #setSequence(Sequence)
	 * @see org.thirdeye.core.persistence.PersistencePackage#getSimulationRun_Sequence()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Sequence getSequence();

	/**
	 * Sets the value of the '{@link org.thirdeye.core.persistence.SimulationRun#getSequence <em>Sequence</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sequence</em>' containment reference.
	 * @see #getSequence()
	 * @generated
	 */
	void setSequence(Sequence value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='long nextStop = getSteps() + currentStep;\ndo {\n\tgetSequence().step();\n\tif( getPersistence() != null) {\n\t\tEList<PersistedNode> models = new BasicEList<PersistedNode>();\n\t\tfor( Entry entry: getSequence().getMesh().getNodes() ) {\n\t\t\tNode node = (Node) entry.getValue();\n\t\t\tPersistedNode persisted = PersistenceFactory.eINSTANCE.createPersistedNode();\n\t\t\tpersisted.setNode((Node) EcoreUtil.copy(node));\n\t\t\tpersisted.setSimulationRun(this);\n\t\t\tpersisted.setSimulationStep(currentStep+1);\n\t\t\tmodels.add(persisted);\n\t\t}\n\t\tgetPersistence().save(models.toArray());\n\t}\n\tfinal Mesh orig = getOriginalMesh();\n\tif( orig != null ) {\n\t\t/*TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(orig);\n\t\tif (domain != null) {\n\t\t\tdomain.getCommandStack().execute(new RecordingCommand(domain) {\n\t\t\t\t   public void doExecute() {\n\t\t\t\t\t\torig.sync(getSequence().getMesh());\n\t\t\t\t   }\n\t\t\t\t});\n\t\t}\052/\n\t}\n\tcurrentStep++;\n} while( isRunning() && (currentStep < nextStop ));\nif( getPersistence() != null) {\n\tgetPersistence().doSave();\n}'"
	 * @generated
	 */
	void run();

} // SimulationRun
