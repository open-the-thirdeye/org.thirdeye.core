/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.persistence;

import org.eclipse.emf.ecore.resource.Resource;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XML Backend</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.thirdeye.core.persistence.PersistencePackage#getXMLBackend()
 * @model
 * @generated
 */
public interface XMLBackend extends Backend {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='String uuid = UUID.randomUUID().toString();\nString fileName = \"test_\" + uuid;\nString fileExtension = \"run\";\nString projectName = \"persistence\";\n\nResource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;\nMap<String, Object> m = reg.getExtensionToFactoryMap();\nm.put(fileExtension, new XMIResourceFactoryImpl());\n\nResourceSet resSet = new ResourceSetImpl();\nIWorkspaceRoot myWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();\n\nIProject myProject = myWorkspaceRoot.getProject(projectName);\nif(myProject.exists() && !myProject.isOpen()) {\n\ttry {\n\t\tmyProject.open(null);\n\t} catch (CoreException e) {\n\t\t// TODO Auto-generated catch block\n\t\te.printStackTrace();\n\t}\n}\nIFile myFile = myProject.getFile(fileName + \".\" + fileExtension);\nreturn resSet.createResource(URI.createPlatformResourceURI(myFile.getFullPath().toString(), true));'"
	 * @generated
	 */
	Resource createResource();
} // XMLBackend
