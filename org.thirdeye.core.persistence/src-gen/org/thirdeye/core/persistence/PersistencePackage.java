/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.persistence;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.thirdeye.core.dependencies.DependenciesPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.thirdeye.core.persistence.PersistenceFactory
 * @model kind="package"
 * @generated
 */
public interface PersistencePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "persistence";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://persistence/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "persistence";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PersistencePackage eINSTANCE = org.thirdeye.core.persistence.impl.PersistencePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.thirdeye.core.persistence.impl.PersistedNodeImpl <em>Persisted Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.persistence.impl.PersistedNodeImpl
	 * @see org.thirdeye.core.persistence.impl.PersistencePackageImpl#getPersistedNode()
	 * @generated
	 */
	int PERSISTED_NODE = 0;

	/**
	 * The feature id for the '<em><b>Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSISTED_NODE__NODE = 0;

	/**
	 * The feature id for the '<em><b>Simulation Step</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSISTED_NODE__SIMULATION_STEP = 1;

	/**
	 * The number of structural features of the '<em>Persisted Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSISTED_NODE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Persisted Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSISTED_NODE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.thirdeye.core.persistence.impl.PersistenceImpl <em>Persistence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.persistence.impl.PersistenceImpl
	 * @see org.thirdeye.core.persistence.impl.PersistencePackageImpl#getPersistence()
	 * @generated
	 */
	int PERSISTENCE = 1;

	/**
	 * The feature id for the '<em><b>Backend</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSISTENCE__BACKEND = 0;

	/**
	 * The number of structural features of the '<em>Persistence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSISTENCE_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Save</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSISTENCE___SAVE__OBJECT = 0;

	/**
	 * The operation id for the '<em>Load</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSISTENCE___LOAD = 1;

	/**
	 * The operation id for the '<em>Get Resource</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSISTENCE___GET_RESOURCE = 2;

	/**
	 * The operation id for the '<em>Do Save</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSISTENCE___DO_SAVE = 3;

	/**
	 * The number of operations of the '<em>Persistence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSISTENCE_OPERATION_COUNT = 4;

	/**
	 * The meta object id for the '{@link org.thirdeye.core.persistence.impl.BackendImpl <em>Backend</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.persistence.impl.BackendImpl
	 * @see org.thirdeye.core.persistence.impl.PersistencePackageImpl#getBackend()
	 * @generated
	 */
	int BACKEND = 2;

	/**
	 * The feature id for the '<em><b>Local Resource</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BACKEND__LOCAL_RESOURCE = 0;

	/**
	 * The number of structural features of the '<em>Backend</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BACKEND_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Create Resource</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BACKEND___CREATE_RESOURCE = 0;

	/**
	 * The operation id for the '<em>Get Stored Nodes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BACKEND___GET_STORED_NODES__VARIABLE_STRING = 1;

	/**
	 * The operation id for the '<em>Get Stored Meshes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BACKEND___GET_STORED_MESHES = 2;

	/**
	 * The operation id for the '<em>Get Stored Runners</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BACKEND___GET_STORED_RUNNERS = 3;

	/**
	 * The operation id for the '<em>Store Mesh</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BACKEND___STORE_MESH__MESH = 4;

	/**
	 * The operation id for the '<em>Store Runner</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BACKEND___STORE_RUNNER__EOBJECT = 5;

	/**
	 * The number of operations of the '<em>Backend</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BACKEND_OPERATION_COUNT = 6;

	/**
	 * The meta object id for the '{@link org.thirdeye.core.persistence.impl.XMLBackendImpl <em>XML Backend</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.persistence.impl.XMLBackendImpl
	 * @see org.thirdeye.core.persistence.impl.PersistencePackageImpl#getXMLBackend()
	 * @generated
	 */
	int XML_BACKEND = 3;

	/**
	 * The feature id for the '<em><b>Local Resource</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_BACKEND__LOCAL_RESOURCE = BACKEND__LOCAL_RESOURCE;

	/**
	 * The number of structural features of the '<em>XML Backend</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_BACKEND_FEATURE_COUNT = BACKEND_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Stored Nodes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_BACKEND___GET_STORED_NODES__VARIABLE_STRING = BACKEND___GET_STORED_NODES__VARIABLE_STRING;

	/**
	 * The operation id for the '<em>Get Stored Meshes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_BACKEND___GET_STORED_MESHES = BACKEND___GET_STORED_MESHES;

	/**
	 * The operation id for the '<em>Get Stored Runners</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_BACKEND___GET_STORED_RUNNERS = BACKEND___GET_STORED_RUNNERS;

	/**
	 * The operation id for the '<em>Store Mesh</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_BACKEND___STORE_MESH__MESH = BACKEND___STORE_MESH__MESH;

	/**
	 * The operation id for the '<em>Store Runner</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_BACKEND___STORE_RUNNER__EOBJECT = BACKEND___STORE_RUNNER__EOBJECT;

	/**
	 * The operation id for the '<em>Create Resource</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_BACKEND___CREATE_RESOURCE = BACKEND_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>XML Backend</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_BACKEND_OPERATION_COUNT = BACKEND_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.core.persistence.impl.SimulationRunImpl <em>Simulation Run</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.persistence.impl.SimulationRunImpl
	 * @see org.thirdeye.core.persistence.impl.PersistencePackageImpl#getSimulationRun()
	 * @generated
	 */
	int SIMULATION_RUN = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_RUN__NAME = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_RUN__DESCRIPTION = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Git Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_RUN__GIT_VERSION = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_RUN__START_TIME = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Steps</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_RUN__STEPS = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Running</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_RUN__RUNNING = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_RUN__SEQUENCE = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Simulation Run</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_RUN_FEATURE_COUNT = DependenciesPackage.ERUNNABLE_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_RUN___RUN = DependenciesPackage.ERUNNABLE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Simulation Run</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_RUN_OPERATION_COUNT = DependenciesPackage.ERUNNABLE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '<em>EJava Object Array</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.persistence.impl.PersistencePackageImpl#getEJavaObjectArray()
	 * @generated
	 */
	int EJAVA_OBJECT_ARRAY = 5;


	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.persistence.PersistedNode <em>Persisted Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Persisted Node</em>'.
	 * @see org.thirdeye.core.persistence.PersistedNode
	 * @generated
	 */
	EClass getPersistedNode();

	/**
	 * Returns the meta object for the containment reference '{@link org.thirdeye.core.persistence.PersistedNode#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Node</em>'.
	 * @see org.thirdeye.core.persistence.PersistedNode#getNode()
	 * @see #getPersistedNode()
	 * @generated
	 */
	EReference getPersistedNode_Node();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.persistence.PersistedNode#getSimulationStep <em>Simulation Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Simulation Step</em>'.
	 * @see org.thirdeye.core.persistence.PersistedNode#getSimulationStep()
	 * @see #getPersistedNode()
	 * @generated
	 */
	EAttribute getPersistedNode_SimulationStep();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.persistence.Persistence <em>Persistence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Persistence</em>'.
	 * @see org.thirdeye.core.persistence.Persistence
	 * @generated
	 */
	EClass getPersistence();

	/**
	 * Returns the meta object for the containment reference '{@link org.thirdeye.core.persistence.Persistence#getBackend <em>Backend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Backend</em>'.
	 * @see org.thirdeye.core.persistence.Persistence#getBackend()
	 * @see #getPersistence()
	 * @generated
	 */
	EReference getPersistence_Backend();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.persistence.Persistence#save(java.lang.Object[]) <em>Save</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Save</em>' operation.
	 * @see org.thirdeye.core.persistence.Persistence#save(java.lang.Object[])
	 * @generated
	 */
	EOperation getPersistence__Save__Object();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.persistence.Persistence#load() <em>Load</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Load</em>' operation.
	 * @see org.thirdeye.core.persistence.Persistence#load()
	 * @generated
	 */
	EOperation getPersistence__Load();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.persistence.Persistence#getResource() <em>Get Resource</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Resource</em>' operation.
	 * @see org.thirdeye.core.persistence.Persistence#getResource()
	 * @generated
	 */
	EOperation getPersistence__GetResource();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.persistence.Persistence#doSave() <em>Do Save</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Save</em>' operation.
	 * @see org.thirdeye.core.persistence.Persistence#doSave()
	 * @generated
	 */
	EOperation getPersistence__DoSave();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.persistence.Backend <em>Backend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Backend</em>'.
	 * @see org.thirdeye.core.persistence.Backend
	 * @generated
	 */
	EClass getBackend();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.persistence.Backend#getLocalResource <em>Local Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Local Resource</em>'.
	 * @see org.thirdeye.core.persistence.Backend#getLocalResource()
	 * @see #getBackend()
	 * @generated
	 */
	EAttribute getBackend_LocalResource();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.persistence.Backend#createResource() <em>Create Resource</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Resource</em>' operation.
	 * @see org.thirdeye.core.persistence.Backend#createResource()
	 * @generated
	 */
	EOperation getBackend__CreateResource();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.persistence.Backend#getStoredNodes(org.thirdeye.core.mesh.Variable, java.lang.String) <em>Get Stored Nodes</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Stored Nodes</em>' operation.
	 * @see org.thirdeye.core.persistence.Backend#getStoredNodes(org.thirdeye.core.mesh.Variable, java.lang.String)
	 * @generated
	 */
	EOperation getBackend__GetStoredNodes__Variable_String();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.persistence.Backend#getStoredMeshes() <em>Get Stored Meshes</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Stored Meshes</em>' operation.
	 * @see org.thirdeye.core.persistence.Backend#getStoredMeshes()
	 * @generated
	 */
	EOperation getBackend__GetStoredMeshes();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.persistence.Backend#getStoredRunners() <em>Get Stored Runners</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Stored Runners</em>' operation.
	 * @see org.thirdeye.core.persistence.Backend#getStoredRunners()
	 * @generated
	 */
	EOperation getBackend__GetStoredRunners();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.persistence.Backend#storeMesh(org.thirdeye.core.mesh.Mesh) <em>Store Mesh</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Store Mesh</em>' operation.
	 * @see org.thirdeye.core.persistence.Backend#storeMesh(org.thirdeye.core.mesh.Mesh)
	 * @generated
	 */
	EOperation getBackend__StoreMesh__Mesh();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.persistence.Backend#storeRunner(org.eclipse.emf.ecore.EObject) <em>Store Runner</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Store Runner</em>' operation.
	 * @see org.thirdeye.core.persistence.Backend#storeRunner(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	EOperation getBackend__StoreRunner__EObject();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.persistence.XMLBackend <em>XML Backend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XML Backend</em>'.
	 * @see org.thirdeye.core.persistence.XMLBackend
	 * @generated
	 */
	EClass getXMLBackend();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.persistence.XMLBackend#createResource() <em>Create Resource</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Resource</em>' operation.
	 * @see org.thirdeye.core.persistence.XMLBackend#createResource()
	 * @generated
	 */
	EOperation getXMLBackend__CreateResource();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.core.persistence.SimulationRun <em>Simulation Run</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simulation Run</em>'.
	 * @see org.thirdeye.core.persistence.SimulationRun
	 * @generated
	 */
	EClass getSimulationRun();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.persistence.SimulationRun#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.thirdeye.core.persistence.SimulationRun#getName()
	 * @see #getSimulationRun()
	 * @generated
	 */
	EAttribute getSimulationRun_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.persistence.SimulationRun#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.thirdeye.core.persistence.SimulationRun#getDescription()
	 * @see #getSimulationRun()
	 * @generated
	 */
	EAttribute getSimulationRun_Description();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.persistence.SimulationRun#getGitVersion <em>Git Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Git Version</em>'.
	 * @see org.thirdeye.core.persistence.SimulationRun#getGitVersion()
	 * @see #getSimulationRun()
	 * @generated
	 */
	EAttribute getSimulationRun_GitVersion();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.persistence.SimulationRun#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see org.thirdeye.core.persistence.SimulationRun#getStartTime()
	 * @see #getSimulationRun()
	 * @generated
	 */
	EAttribute getSimulationRun_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.persistence.SimulationRun#getSteps <em>Steps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Steps</em>'.
	 * @see org.thirdeye.core.persistence.SimulationRun#getSteps()
	 * @see #getSimulationRun()
	 * @generated
	 */
	EAttribute getSimulationRun_Steps();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.core.persistence.SimulationRun#isRunning <em>Running</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Running</em>'.
	 * @see org.thirdeye.core.persistence.SimulationRun#isRunning()
	 * @see #getSimulationRun()
	 * @generated
	 */
	EAttribute getSimulationRun_Running();

	/**
	 * Returns the meta object for the containment reference '{@link org.thirdeye.core.persistence.SimulationRun#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sequence</em>'.
	 * @see org.thirdeye.core.persistence.SimulationRun#getSequence()
	 * @see #getSimulationRun()
	 * @generated
	 */
	EReference getSimulationRun_Sequence();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.core.persistence.SimulationRun#run() <em>Run</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Run</em>' operation.
	 * @see org.thirdeye.core.persistence.SimulationRun#run()
	 * @generated
	 */
	EOperation getSimulationRun__Run();

	/**
	 * Returns the meta object for data type '<em>EJava Object Array</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EJava Object Array</em>'.
	 * @model instanceClass="java.lang.Object[]" serializeable="false"
	 * @generated
	 */
	EDataType getEJavaObjectArray();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PersistenceFactory getPersistenceFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.thirdeye.core.persistence.impl.PersistedNodeImpl <em>Persisted Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.persistence.impl.PersistedNodeImpl
		 * @see org.thirdeye.core.persistence.impl.PersistencePackageImpl#getPersistedNode()
		 * @generated
		 */
		EClass PERSISTED_NODE = eINSTANCE.getPersistedNode();

		/**
		 * The meta object literal for the '<em><b>Node</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSISTED_NODE__NODE = eINSTANCE.getPersistedNode_Node();

		/**
		 * The meta object literal for the '<em><b>Simulation Step</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSISTED_NODE__SIMULATION_STEP = eINSTANCE.getPersistedNode_SimulationStep();

		/**
		 * The meta object literal for the '{@link org.thirdeye.core.persistence.impl.PersistenceImpl <em>Persistence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.persistence.impl.PersistenceImpl
		 * @see org.thirdeye.core.persistence.impl.PersistencePackageImpl#getPersistence()
		 * @generated
		 */
		EClass PERSISTENCE = eINSTANCE.getPersistence();

		/**
		 * The meta object literal for the '<em><b>Backend</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSISTENCE__BACKEND = eINSTANCE.getPersistence_Backend();

		/**
		 * The meta object literal for the '<em><b>Save</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PERSISTENCE___SAVE__OBJECT = eINSTANCE.getPersistence__Save__Object();

		/**
		 * The meta object literal for the '<em><b>Load</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PERSISTENCE___LOAD = eINSTANCE.getPersistence__Load();

		/**
		 * The meta object literal for the '<em><b>Get Resource</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PERSISTENCE___GET_RESOURCE = eINSTANCE.getPersistence__GetResource();

		/**
		 * The meta object literal for the '<em><b>Do Save</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PERSISTENCE___DO_SAVE = eINSTANCE.getPersistence__DoSave();

		/**
		 * The meta object literal for the '{@link org.thirdeye.core.persistence.impl.BackendImpl <em>Backend</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.persistence.impl.BackendImpl
		 * @see org.thirdeye.core.persistence.impl.PersistencePackageImpl#getBackend()
		 * @generated
		 */
		EClass BACKEND = eINSTANCE.getBackend();

		/**
		 * The meta object literal for the '<em><b>Local Resource</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BACKEND__LOCAL_RESOURCE = eINSTANCE.getBackend_LocalResource();

		/**
		 * The meta object literal for the '<em><b>Create Resource</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BACKEND___CREATE_RESOURCE = eINSTANCE.getBackend__CreateResource();

		/**
		 * The meta object literal for the '<em><b>Get Stored Nodes</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BACKEND___GET_STORED_NODES__VARIABLE_STRING = eINSTANCE.getBackend__GetStoredNodes__Variable_String();

		/**
		 * The meta object literal for the '<em><b>Get Stored Meshes</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BACKEND___GET_STORED_MESHES = eINSTANCE.getBackend__GetStoredMeshes();

		/**
		 * The meta object literal for the '<em><b>Get Stored Runners</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BACKEND___GET_STORED_RUNNERS = eINSTANCE.getBackend__GetStoredRunners();

		/**
		 * The meta object literal for the '<em><b>Store Mesh</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BACKEND___STORE_MESH__MESH = eINSTANCE.getBackend__StoreMesh__Mesh();

		/**
		 * The meta object literal for the '<em><b>Store Runner</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BACKEND___STORE_RUNNER__EOBJECT = eINSTANCE.getBackend__StoreRunner__EObject();

		/**
		 * The meta object literal for the '{@link org.thirdeye.core.persistence.impl.XMLBackendImpl <em>XML Backend</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.persistence.impl.XMLBackendImpl
		 * @see org.thirdeye.core.persistence.impl.PersistencePackageImpl#getXMLBackend()
		 * @generated
		 */
		EClass XML_BACKEND = eINSTANCE.getXMLBackend();

		/**
		 * The meta object literal for the '<em><b>Create Resource</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XML_BACKEND___CREATE_RESOURCE = eINSTANCE.getXMLBackend__CreateResource();

		/**
		 * The meta object literal for the '{@link org.thirdeye.core.persistence.impl.SimulationRunImpl <em>Simulation Run</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.persistence.impl.SimulationRunImpl
		 * @see org.thirdeye.core.persistence.impl.PersistencePackageImpl#getSimulationRun()
		 * @generated
		 */
		EClass SIMULATION_RUN = eINSTANCE.getSimulationRun();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_RUN__NAME = eINSTANCE.getSimulationRun_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_RUN__DESCRIPTION = eINSTANCE.getSimulationRun_Description();

		/**
		 * The meta object literal for the '<em><b>Git Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_RUN__GIT_VERSION = eINSTANCE.getSimulationRun_GitVersion();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_RUN__START_TIME = eINSTANCE.getSimulationRun_StartTime();

		/**
		 * The meta object literal for the '<em><b>Steps</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_RUN__STEPS = eINSTANCE.getSimulationRun_Steps();

		/**
		 * The meta object literal for the '<em><b>Running</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_RUN__RUNNING = eINSTANCE.getSimulationRun_Running();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_RUN__SEQUENCE = eINSTANCE.getSimulationRun_Sequence();

		/**
		 * The meta object literal for the '<em><b>Run</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SIMULATION_RUN___RUN = eINSTANCE.getSimulationRun__Run();

		/**
		 * The meta object literal for the '<em>EJava Object Array</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.persistence.impl.PersistencePackageImpl#getEJavaObjectArray()
		 * @generated
		 */
		EDataType EJAVA_OBJECT_ARRAY = eINSTANCE.getEJavaObjectArray();

	}

} //PersistencePackage
