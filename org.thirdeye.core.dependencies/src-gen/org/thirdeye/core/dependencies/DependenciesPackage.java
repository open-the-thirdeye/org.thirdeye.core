/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.core.dependencies;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.thirdeye.core.dependencies.DependenciesFactory
 * @model kind="package"
 * @generated
 */
public interface DependenciesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "dependencies";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://dependencies/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "dependencies";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DependenciesPackage eINSTANCE = org.thirdeye.core.dependencies.impl.DependenciesPackageImpl.init();

	/**
	 * The meta object id for the '{@link java.lang.Runnable <em>ERunnable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.Runnable
	 * @see org.thirdeye.core.dependencies.impl.DependenciesPackageImpl#getERunnable()
	 * @generated
	 */
	int ERUNNABLE = 0;

	/**
	 * The number of structural features of the '<em>ERunnable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERUNNABLE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>ERunnable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERUNNABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.apache.commons.math3.ode.FirstOrderDifferentialEquations <em>EFirst Order Differential Equations</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.apache.commons.math3.ode.FirstOrderDifferentialEquations
	 * @see org.thirdeye.core.dependencies.impl.DependenciesPackageImpl#getEFirstOrderDifferentialEquations()
	 * @generated
	 */
	int EFIRST_ORDER_DIFFERENTIAL_EQUATIONS = 1;

	/**
	 * The number of structural features of the '<em>EFirst Order Differential Equations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFIRST_ORDER_DIFFERENTIAL_EQUATIONS_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>EFirst Order Differential Equations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFIRST_ORDER_DIFFERENTIAL_EQUATIONS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '<em>ECyclic Barrier</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.concurrent.CyclicBarrier
	 * @see org.thirdeye.core.dependencies.impl.DependenciesPackageImpl#getECyclicBarrier()
	 * @generated
	 */
	int ECYCLIC_BARRIER = 2;

	/**
	 * The meta object id for the '<em>EFirst Order Integrator</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.apache.commons.math3.ode.FirstOrderIntegrator
	 * @see org.thirdeye.core.dependencies.impl.DependenciesPackageImpl#getEFirstOrderIntegrator()
	 * @generated
	 */
	int EFIRST_ORDER_INTEGRATOR = 3;


	/**
	 * The meta object id for the '<em>EDouble Array</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.core.dependencies.impl.DependenciesPackageImpl#getEDoubleArray()
	 * @generated
	 */
	int EDOUBLE_ARRAY = 4;


	/**
	 * Returns the meta object for class '{@link java.lang.Runnable <em>ERunnable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ERunnable</em>'.
	 * @see java.lang.Runnable
	 * @model instanceClass="java.lang.Runnable"
	 * @generated
	 */
	EClass getERunnable();

	/**
	 * Returns the meta object for class '{@link org.apache.commons.math3.ode.FirstOrderDifferentialEquations <em>EFirst Order Differential Equations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EFirst Order Differential Equations</em>'.
	 * @see org.apache.commons.math3.ode.FirstOrderDifferentialEquations
	 * @model instanceClass="org.apache.commons.math3.ode.FirstOrderDifferentialEquations"
	 * @generated
	 */
	EClass getEFirstOrderDifferentialEquations();

	/**
	 * Returns the meta object for data type '{@link java.util.concurrent.CyclicBarrier <em>ECyclic Barrier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ECyclic Barrier</em>'.
	 * @see java.util.concurrent.CyclicBarrier
	 * @model instanceClass="java.util.concurrent.CyclicBarrier"
	 * @generated
	 */
	EDataType getECyclicBarrier();

	/**
	 * Returns the meta object for data type '{@link org.apache.commons.math3.ode.FirstOrderIntegrator <em>EFirst Order Integrator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EFirst Order Integrator</em>'.
	 * @see org.apache.commons.math3.ode.FirstOrderIntegrator
	 * @model instanceClass="org.apache.commons.math3.ode.FirstOrderIntegrator"
	 * @generated
	 */
	EDataType getEFirstOrderIntegrator();

	/**
	 * Returns the meta object for data type '<em>EDouble Array</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EDouble Array</em>'.
	 * @model instanceClass="double[]"
	 * @generated
	 */
	EDataType getEDoubleArray();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DependenciesFactory getDependenciesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link java.lang.Runnable <em>ERunnable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.Runnable
		 * @see org.thirdeye.core.dependencies.impl.DependenciesPackageImpl#getERunnable()
		 * @generated
		 */
		EClass ERUNNABLE = eINSTANCE.getERunnable();

		/**
		 * The meta object literal for the '{@link org.apache.commons.math3.ode.FirstOrderDifferentialEquations <em>EFirst Order Differential Equations</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.apache.commons.math3.ode.FirstOrderDifferentialEquations
		 * @see org.thirdeye.core.dependencies.impl.DependenciesPackageImpl#getEFirstOrderDifferentialEquations()
		 * @generated
		 */
		EClass EFIRST_ORDER_DIFFERENTIAL_EQUATIONS = eINSTANCE.getEFirstOrderDifferentialEquations();

		/**
		 * The meta object literal for the '<em>ECyclic Barrier</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.concurrent.CyclicBarrier
		 * @see org.thirdeye.core.dependencies.impl.DependenciesPackageImpl#getECyclicBarrier()
		 * @generated
		 */
		EDataType ECYCLIC_BARRIER = eINSTANCE.getECyclicBarrier();

		/**
		 * The meta object literal for the '<em>EFirst Order Integrator</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.apache.commons.math3.ode.FirstOrderIntegrator
		 * @see org.thirdeye.core.dependencies.impl.DependenciesPackageImpl#getEFirstOrderIntegrator()
		 * @generated
		 */
		EDataType EFIRST_ORDER_INTEGRATOR = eINSTANCE.getEFirstOrderIntegrator();

		/**
		 * The meta object literal for the '<em>EDouble Array</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.core.dependencies.impl.DependenciesPackageImpl#getEDoubleArray()
		 * @generated
		 */
		EDataType EDOUBLE_ARRAY = eINSTANCE.getEDoubleArray();

	}

} //DependenciesPackage
